# CHANGELOG

## 2024-11-11

- #57 Added `$WAIT_TIME` variable to `Update Firewall` script before sending email

## 2024-10-27

- #54 Updated `Code-Backup` to include unlimited remotes, improved logging and notifications

## 2024-10-19

- #53 Fixed bug with kernel listing and improved removal logic in Remove Kernels Script

## 2024-10-16

- #28 Add Better Mode Selection to Scripts in the Repository

## 2024-10-15

- #52 Added Ability to Update Multiple Firewalls in Update Firewall Script

## 2024-10-13

- #51 Added Extra Modes to Update Firewall Script (Verbose, No-Output Test) and updated logging

## 2024-10-12

- #50 Implemented `NTFY-cURL`/ custom commands notifications and updated instructions

## 2024-10-10

- #49 Improved `NTFY-cURL` script env handling / added `--test` option

## 2024-10-08

- #46 Added `Remove Kernels` script

## 2024-10-06

- #48 Added Check Tailscale Status script
- #47 Added `NTFY-cURL` script to send notifications / emails using cURL

## 2024-02-12

- #23 Add CIDR Support to Update Firewall Script
- #31 Added IP Verification to Update Firewall Script
- #29 Added ability to pause/unpause monitor-groups in Web Check script
- #45 Updated API Scripts to use the same framework and conventions

## 2024-01-29

- #44 Rename/Reorganise Scripts and sample env files

## 2024-01-26

- #42 Added option to get acl from multiple tailnets at once
- #43 Updated Tailscale Script to use OAuth Token, added functions, improved logic check

## 2023-10-20

- #41 Updated Linux Package List to dpkg

## 2023-10-19

- #40 Fixed bug with code backup script in which copying locally would not work
- #39 Modularised Scripts

## 2023-09-26

- #36 Added New Script to Download Tailscale ACL for Backup
- #38 Updated Scripts with New Formatting and Linting

## 2023-09-24

- #37 Added new encrypt-screenshots script to encrypt files using GPG

## 2023-09-11

- #35 Added `dirname "$0"` to all scripts to make them more portable

## 2023-08-03

- #34 Restructured main README.md to be more informative

## 2023-07-13

- #33 Closed issue about ansible automation as out of scope for the project - will be in a separate repository

## 2023-06-24

- #32 Updated reference to Better Stack Uptime in web-monitor-pause script

## 2023-04-30

- #30 Added linting to all scripts across repo

## 2023-04-25

- #25 Expanded functionality of error information in web monitor pause script
- #24 (REOPEN) Updated web monitor arrays to make it easier to add new monitors as well as corresponding documentation

## 2023-04-24

- #24 Updated web monitor pause so works using arrays making it more efficient and infinitely scalable

## 2023-04-23

- #19 Add notification to tell when IP has actually changed to another IP

## 2023-04-22

- #27 Added "first-run" option to check unencrypted, uncompressed files locally before running script in production

## 2023-04-21

- #26 Restructured repository

## 2023-04-20

- #18 Add custom commands to code backup script

## 2023-04-05

- #20 Setting up Ansible Script for Repo
- #22 Update CP Command to preserve attributes

## 2023-04-04

- #21 Add Crontab Copy option to Backup Script

## 2023-03-19

- #11 Added option to input a single IPv4 address

## 2023-03-18

- #10 Make monitor ID gathering system like uptime script
- #17 Add options to code backup script

## 2023-03-17

- #16 Added Readme and renamed cron to extra
- #15 (Reopen) to add cryttab

## 2023-03-16

- #15 Updated code-backup script with fstab option

## 2023-03-15

- #14 Update code validation and fix up variables in backup/code-backup script

## 2023-03-14

- #9 Updated scripts to include echo -e
- #13 Add .env Verification
- #12 Digital Ocean Bug Fix API Verification

## 2023-03-13

- #7 Digital Ocean/CLoudflare Firewall/ACL update on dynamically allocated IP

## 2023-03-11

- #8 Added inbuilt monitor ID search in better uptime script for first run

## 2023-03-08

- #6 Updating .env to new naming scheme

## 2023-03-07

- #5 Adding Monitor Active Validation to Better-Uptime API Pause Script
- #3 Better Uptime API Pause/Unpause script

## 2023-03-06

- #4 Update CHANGELOG, .gitignore and add logs directory

## 2023-03-05

- #2 MacOS code folder backup

## 2023-03-01

- #1 Setting up scripts directory
