# Encryption Directory

This directory contains scripts for encrypting and decrypting files using various encryption services/methods.

## Scripts

### Encrypt Screenshots

This script is for encrypting screenshots taken on a Linux/MacOS. It uses `gpg` to encrypt the files and `rm` to remove the original files. This script is useful if you want to keep your screenshots private.
