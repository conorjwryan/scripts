#!/bin/bash
# shellcheck source=/dev/null
# Daily Backup Script for Project Ara
# Location: encrypt/encrypt-screenshots/encrypt-screenshots.sh
# Version 1.0.0
# Author: Conor Ryan
# Date: 2021-09-26
# Description: This script will encrypt all files in the specified directory. Ideal for encrypting screenshots.
####

####
# Script Variables
####
# Internal Variables defined in script (lowercase)
####
# file - file to be encrypted in the specified directory
####
# Variables defined in .env file (uppercase)
####
# GPG_EMAIL - email address associated with the GPG key to be used for encryption
# SCREENSHOT_DIR - directory containing the files to be encrypted
####

####
# Check if script is being run as root
####
# To ensure that users cannot accidentally encrypt their protected files
# If the script is run as root, it will abort
####

if [[ $EUID -eq 0 ]]; then
   echo "This script must not be run as root" 
   exit 1
fi

####
# Change Directory to Script Directory
####

cd "$(dirname "$0")" || exit 1

echo -e "Starting Encryption Script\n";

####
# START SCRIPT PRE-REQUISITES
####
# This part of the script will work to ensure that the script has everything it needs to run
# If any of the pre-requisites are not met, the script will abort

####
# Load .env file
####

if [[ ! -f ".encrypt-screenshots.env" ]]; then
	echo -e '.env file not found. Aborting script.'
	exit 1;
fi

set -o allexport
source .encrypt-screenshots.env
set +o allexport
set -e

####
# Check for GPG Public Key for Encryption
####
# This section will check for the existence of the GPG public key specified in the .env file
# If the key does not exist, the script will abort
####

echo -e "Checking for GPG Key\n$GPG_EMAIL\n";

if gpg --list-keys "${GPG_EMAIL}" > /dev/null 2>&1; then
	echo -e "GPG Key found\n";
else
	echo -e "GPG Key not found, encryption cannot be done. Aborting script.\n";
	exit 1;
fi

####
# Check for Screenshot Directory
####

echo -e "Checking for Specified Directory\n"
echo -e "${SCREENSHOT_DIR}\n";

if [[ ! -d "${SCREENSHOT_DIR}" ]]; then
	echo -e 'Directory does not exist.\nPlease check the specified path in the .env file.\n Aborting script.'
	exit 1;
fi

####
# END SCRIPT PRE-REQUISITES
####

####
# Encrypt Screenshots
####
# Assumes that the GPG key is already imported into the host system
# Checks for existence of public key at the start of the script
####
# This section will encrypt all files in the specified directory
# If the file is already encrypted, it will be skipped
# The script will then remove the non-encrypted file after encryption
####

echo -e "Start Encryption\n";

for file in "${SCREENSHOT_DIR}"*; do

	if [[ $file != *.asc ]]; then
		echo -e "Processing $file";
		gpg --encrypt --armor  --recipient "${GPG_EMAIL}" "$file"
		rm -r "$file"
		echo -e "Removed non-encrypted files\n";

	else 
		echo -e "Skipping $file\n. Already encrypted.";

	fi
done

echo -e "Finished Encryption\n";
