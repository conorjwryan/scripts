# encrypt-screenshots.sh

A bash script to encrypt screenshots using GPG.

## Description

The main function of this script is to encrypt screenshots using GPG. However, this script can be used to encrypt any file or directory as defined by the `SCREENSHOT_DIR` variable in the `.encrypt-screenshots.env` file.

## Requirements

1. GPG must be installed on the system.

    In most Linux distributions GPG is installed by default. If it is not installed, install it using the package manager for your distribution.

    For example, in Ubuntu:

    ```bash
    sudo apt install gnupg
    ```

    For MacOS use Homebrew:

    ```bash
    brew install gnupg
    ```

2. A GPG key pair is created for encryption.

    If you do not have a GPG key pair, create one using the following command:

    ```bash
    gpg --full-generate-key
    ```

    Follow the prompts to create the key pair.

    It will create ask for an email recipient, which doesn't have to be real especially if you're using this script internally.

    The email address used to create the key pair must match the email address used in the `GPG_EMAIL` variable in the `.encrypt-screenshots.env` file.

## Setup

1. In the `encrypt-screenshots` directory copy the `sample.encrypt-screenshots.env` file to `.encrypt-screenshots.env` and edit open the file in a text editor.

    ```bash
    cp sample.encrypt-screenshots.env .encrypt-screenshots.env
    ```

    ```bash
    nano .encrypt-screenshots.env
    ```

2. Edit the input the following variables:

    ```bash
    # The GPG recipient email address - see previous section for details.
    GPG_EMAIL="";

    # The directory where the screenshots are stored.
    SCREENSHOT_DIR="~/Screenshots"
    ```

3. Make the script executable.

    ```bash
    chmod +x encrypt-screenshots.sh
    ```

## Usage

This script cannot be run as root or with sudo. This is to protect important system files from being accidentally encrypted.

To run the script, execute the following command:

```bash
./encrypt-screenshots.sh
```
