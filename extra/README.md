# Extra Directory

This is the place where you can put the scripts which aren't covered by the repository. Whether that's specific uptime checks of servers or docker install scripts. It's always good to be organised.

Anything you put in here will be ignored by the git repository. Nothing will be tracked, committed or overwritten. It's your space to do what you want with.
