# tailscale-acl.sh

This script is used to get the current ACL for a Tailscale network. Useful if you are updating the ACL and need a backup of the current one before you make changes.

## How to Setup the Script

The following instructions detail the required software, information, and setup required to use this script.

It can be run on both Linux and macOS. For Mac, you will need to install the required software using [Homebrew](https://brew.sh/).

### Required Software

- [Tailscale](https://tailscale.com/) - while this software is not required to be present on the machine you run this script on it is assumed you have already got a Tailscale account and have it installed on at least one machine.

- `cURL` - this script uses `cURL` to make the `API` calls to Tailscale. This is installed by default on most `Linux` distributions and `macOS`. If you do not have it installed, you can install it using your `package manager`.

- jq - this script uses `jq`` to parse the JSON response from the Tailscale API. This may not be installed by default so please double check

If the above software is not installed the script will not run.

### Required Information

In order to run this script, you will need to gather the following information:

- `Tailnet` - this the domain name of your network. like `example.com`
- Authentication Keys - either an `API Access Token` or `OAuth Client` - see below:

There are two ways to authenticate with the Tailscale API: `API Access Token` or `OAuth Client`. While the script supports both, it is recommended to use the `OAuth Client` way as it is more secure.

#### API Access Token

`API Access Tokens` created via the `Web UI` are only valid for up to `90 days` and cannot be scoped so they allow `read/write` access to all of your Tailscale resources. This is not ideal for security reasons.

[Available here](https://login.tailscale.com/admin/settings/keys) when logged into your Tailscale account.

#### OAuth Client (Recommended)

`OAuth Clients` are valid for 1 year and can be scoped to only allow read access to your Tailscale resources. This is the recommended way to authenticate with the Tailscale API.

[Available here](https://login.tailscale.com/admin/settings/oauth) when logged into your Tailscale account.

When creating the `OAuth Client` you need to specify the scope of the client. Choose `read:acl` to only allow read access to your ACLs and nothing else. The `OAuth Client` will then be created and you will be given a `Client ID` and `Client Secret`. You will need these to authenticate with the API.

During the script process the `Oauth Client` authenticates with the server and a specific `API Token` is then generated having the same permissions as the `OAuth Client` but is only valid for 1 hour. This `API Token` is then used in the script to download the `ACL` from an `API` call.

### Script Setup and Configuration

1. Clone this repository to your local machine and run the script from the command line.

    ```bash
    git clone https://gitlab.com/conorjwryan/scripts.git
    ```

    Enter the directory and then copy the `.env.example` file to `.env` and fill in the required information.

    ```bash
    cd scripts/api/tailscale-acl
    ```

2. Create a `.env` file from the sample file `sample.tailscale-acl.env` and open it in your preferred text editor.

    ```bash
    cp sample.tailscale-acl.env tailscale-acl.env
    ```

    ```bash
    nano tailscale-acl.env
    ```

3. Based on the information gathered from "[Required Information](#required-information)" step above fill in the required information in the `.env` file.

    The `.env` file contains `output` file which is used to determine the name and location of the downloaded `ACL` file. The default is file is `tailscale_acl_<date/time>_<tailnet>` which is then placed in the `output` folder. You can change the name of the file and the location of the folder if you wish.

    Only 3/4 variables need to be filled in:

    - `TAILNET` - this is the domain name of your network. like `example.com`

    If you're using the `OAuth Client` method, you will also need to fill in the following:
    - `API_TYPE="oauth"`
    - `API_CLIENT` - this is the `Client ID` of the `OAuth Client` you created above.
    - `API_SECRET` - this is the `Client Secret` of the `OAuth Client` you created above.

    If you're using the `API Access Token` method, you will also need to fill in the following:

    - `API_TYPE="token"`
    - `API_TOKEN` - this is the `API Access Token` you created above.

    Because the script uses Arrays to enable multiple `tailnets` to be downloaded at once, you will need to use the following framework to add multiple `tailnets`:

    ```bash
    TAILNET[0]=""
    API_TYPE[0]=""
    API_CLIENT[0]=""
    API_SECRET[0]=""
    API_TOKEN[0]=""
    ```

    If you want to add more than one `tailnet` simply add another set of variables with the next number in the array:

    ```bash
    TAILNET[1]=""
    API_TYPE[1]=""
    API_CLIENT[1]=""
    API_SECRET[1]=""
    API_TOKEN[1]=""
    ```

    With arrays in bash we start at `0` so the first `tailnet` will be `0`, the second `1`, and so on.

    Once you are done save and close the file.

4. The script should be executable by default but if it is not, make it and it's subscripts executable:

    ```bash
    find . -name "*.sh" -exec chmod +x {} \;
    ```

## How to Use the Script

To use this script, simply run it from the command line.

```bash
./tailscale-acl.sh <ext>
```

Currently the script supports two options:

- `hujson` (or blank) - this is the default option and will output the ACL in a human readable JSON format (preferred by `Tailscale`).
- `json` - this will output the ACL in a single line JSON format.

## Future Improvements

- Better Error Handling - currently even if the API call fails the script will still create an empty file. This will be fixed in future
