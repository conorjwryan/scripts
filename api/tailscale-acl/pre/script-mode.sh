#!/bin/bash
# shellcheck disable=SC2034
# Tailscale ACL Script -  Script Mode
# Location: api/tailscale-acl/pre/script-mode.sh
# Version 1.0.2
# Author: Conor Ryan
# Date: 2024-02-10
# Description: This script checks for the script mode 
####

if [[ -z "$1" || $1 == "hujson" ]]; then
    ext=".hujson"
        
elif [[ $1 == "json" ]]; then
    ext=".json"

else
    log "Invalid argument\nPlease use either 'hujson' or 'json' as an argument";
    log "The script will now exit"
    exit 1

fi