#!/bin/bash
# shellcheck disable=
# Tailscale ACL Script -  Script Checks
# Location: api/tailscale-acl/pre/script-checks.sh
# Version 1.0.2
# Author: Conor Ryan
# Date: 2024-02-10
# Description: This script checks the required packages and commands for the script to run
####

# Check if script is being run as root
if [[ $EUID -eq 0 ]]; then
   log "This script should not be run using sudo or as the root user"
   log "The script will now exit"
   exit 1

fi

# Check for existence of cURL
if ! command -v curl &> /dev/null; then
    log "cURL could not be found";
    log "Please install cURL and try again";
    log "The script will now exit"
    exit 1

fi

# Check for existence of jq
if ! command -v jq &> /dev/null; then
    log "jq could not be found";
    log "Please install jq and try again"
    log "The script will now exit"
    exit 1

fi

# Check for existence of output directory
if [ ! -d "${OUTPUT_DIRECTORY}" ]; then
  log "No output directory found";
  log "Please set the OUTPUT_DIRECTORY variable in the .tailscale.env file";
  log "The script will now try and create the directory"

    # Create output directory on user confirmation
    read -r -p -e "Do you want to create the output directory at:\n${OUTPUT_DIRECTORY}\n[y/N]" response

    if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]; then
        mkdir -p "${OUTPUT_DIRECTORY}"; 
        log "Output directory created";
        log "The script will now continue";
    fi

fi