#!/bin/bash
# shellcheck disable=SC2034,SC2154,SC2086
# Tailscale ACL Script - Loop Process Script
# Location: api/tailscale-acl/main/loop-process.sh
# Version 1.0.2
# Author: Conor Ryan
# Date: 2024-02-10
# Description: The script houses the loop which processes each Tailscale Network
####

for i in "${!TAILNET[@]}"; 
    do
    result=""
    log "Processing ${TAILNET[i]}"

    # API Type is OAuth
    if [[ ${API_TYPE[i]} == "oauth" ]]; then
        log "API Type is OAuth"

        if [[ ${API_CLIENT[i]} == "" || ${API_SECRET[i]} == "" ]]; then
            log "API Client and API Secret not found";
            continue;
        fi

        API_TOKEN[i]=$(oauth_function "${API_CLIENT[i]}" "${API_SECRET[i]}" "${TAILNET[i]}")

    # API Type is Token
    elif [[ ${API_TYPE[i]} == "token" ]]; then
        log "API Type is Token"

        if [[ ${API_TOKEN[i]} == "" ]]; then
            log "API Token not found";
            continue;
        fi

    else
        log "Invalid API Type";
        continue;

    fi

    acl_function "${API_TOKEN[i]}" "${TAILNET[i]}" "${ext}"
    echo ""

done