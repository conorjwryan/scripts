#!/bin/bash
# shellcheck disable=SC1091,SC2034,SC2154
# Tailscale ACL Script - Main Script
# Location: api/tailscale-acl/tailscale-acl.sh
# Version 3.0.2
# Author: Conor Ryan
# Date: 2024-02-10
# Description: Script to download the ACL from Tailscale
####

script_name="tailscale-acl"

# Change Directory to Script Directory
cd "$(dirname "$0")" || exit 1

# Load .env file
if [ ! -f ".tailscale-acl.env" ]; then
  echo -e "No '.tailscale-acl.env' file found"
  echo -e "Make sure that you have copied from the sample file into the script directory first before executing this script"
  exit 1

fi

set -o allexport
source .tailscale-acl.env
set +o allexport

# Add functions
source ./functions/function-list.sh

# Script Prerequisites
source ./pre/script-checks.sh

# Script Mode Selection
source ./pre/script-mode.sh

# For Loop to process each Tailscale Network
source ./main/loop-process.sh

# If script completes without error
exit 0
