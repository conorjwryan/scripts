#!/bin/bash
# shellcheck disable=2034
# Tailscale ACL Script - OAuth Function Script
# Location: api/tailscale-acl/functions/oauth-function.sh
# Version 1.0.1
# Author: Conor Ryan
# Date: 2024-01-26
# Description: The function which runs the API call for the OAuth Token
####

oauth_function() {

    local client_id="$1"
    local client_secret="$2"
    local api_token=""

    oauth_curl=(curl -s -d "client_id=${client_id}" -d "client_secret=${client_secret}" "https://api.tailscale.com/api/v2/oauth/token")

    api_token="$(jq -r '.access_token' <( "${oauth_curl[@]}" ))"

    echo "${api_token}"
}