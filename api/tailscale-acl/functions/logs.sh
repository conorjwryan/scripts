#!/bin/bash
# shellcheck disable=SC1091,SC2154
# Tailscale ACL Script - Logs Function
# Location: api/tailscale-acl/functions/logs.sh
# Version 1.0.0
# Author: Conor Ryan
# Date: 2024-02-10
# Description: Functions for making and generating logs
####

if [ ! -d "${LOG_LOCATION}" ]; then
    mkdir -p "${LOG_LOCATION}" 2>/dev/null

    if [ ! -d "${LOG_LOCATION}" ]; then
        echo -e "Error: Unable to create logs directory"
        echo -e "Error: Check permissions and try again"
        exit 1

    fi

fi

log_file="${LOG_LOCATION}${LOG_NAME}"

if [ ! -f "${log_file}" ]; then
        touch "${log_file}"

        if [ ! -f "${log_file}" ]; then
            echo -e "Error: Unable to create log file"
            echo -e "Error: Check permissions and try again"
            exit 1

        fi

fi

if [[ ${LOG_OVERWRITE} == 1 ]]; then
    echo -n "" > "${log_file}"
fi

log() {
    echo -e "$1";
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" >> "${log_file}"

}

log_api() {
    if [[ ${LOG_LEVEL} == "EXTENDED"  ]]; then
        echo -e "$1";
        echo "";
        echo "$(date '+%Y-%m-%d %H:%M:%S') - $1" >> "${log_file}"

    fi

}
