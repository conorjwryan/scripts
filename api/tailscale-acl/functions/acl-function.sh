#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Tailscale ACL Script -  ACL Function Script
# Location: api/tailscale-acl/functions/acl-function.sh
# Version 1.1.0
# Author: Conor Ryan
# Date: 2024-02-10
# Description: The function which runs the API call for the ACL
####

acl_function() {

    local api_token="$1"
    local tailnet="$2"
    local ext="$3"
    local curl_json=""
    local curl_hujson=""


    if [[ ${ext} == ".json" ]]; then
        curl_json=$(curl -s -H "Authorization: Bearer ${api_token}"  -H "Accept: application/json" -X GET "https://api.tailscale.com/api/v2/tailnet/${tailnet}/acl")
    
        echo "${curl_json}" > "${FULL_OUTPUT_PATH}_${tailnet}${ext}"
        log_api "ACL Response JSON ${tailnet}: ${curl_json}"

    else
        curl_hujson=$(curl -s -H "Authorization: Bearer ${api_token}" -X GET "https://api.tailscale.com/api/v2/tailnet/${tailnet}/acl")

        echo "${curl_hujson}" > "${FULL_OUTPUT_PATH}_${tailnet}${ext}"
        log_api "ACL Response HuJSON ${tailnet}: ${curl_hujson}"

    fi

    full_file_path="$(realpath ${FULL_OUTPUT_PATH}_${tailnet}${ext})"

    # File Creation Check
    if [ ! -f "${FULL_OUTPUT_PATH}_${tailnet}${ext}" ]; then
        log "ERROR: File not created";
        log "ERROR: This may be due to permissions or the output directory not existing";


    else
        log "File created at: ${full_file_path}";

    fi
}

