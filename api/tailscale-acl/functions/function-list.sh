#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Tailscale ACL Script - Function List
# Location: api/tailscale-acl/functions/function-list.sh
# Version 1.1.0
# Author: Conor Ryan
# Date: 2024-02-10
# Description: Contains the functions for the Tailscale ACL Script
####

source ./functions/logs.sh

source ./functions/oauth-function.sh

source ./functions/acl-function.sh