# API Directory

The scripts in this directory require api keys to be set up in order to work. They work through cURL requests to the relevant API endpoints.

In the interest of both privacy and system agnostic operation any sensitive information (home directories / code folders and credentials) have been obscured and are accessed by the script calling the `.env` file in each of these directories

## Scripts

### Tailscale ACL Download

This script is for those who use [Tailscale](https://tailscale.com/). This script will download the ACLs from Tailscale and save them to a specified location. This is useful if you want to keep a backup of your ACLs before a change.

### Update Firewall

For those who use [Digital Ocean](https://www.digitalocean.com/) and [Cloudflare](https://www.cloudflare.com/) to manage their DNS and firewall, this script will update the firewall to allow the IP address of the machine it is running on. This is useful if you are running a dynamic IP address and want to be able to access your server without having to update the firewall manually.

### Web Check

For those who use [Better Uptime](https://betteruptime.com/) to monitor their websites, this script will pause all monitors for a specified website. This is useful if you are doing maintenance on a website and do not want to be notified of any downtime `incidents`.
