#!/bin/bash
# shellcheck disable=SC1091,SC2154
# Web Check Script
# Location: api/web-check/functions/error-notice.sh
# Version 1.1.0
# Author: Conor Ryan
# Date: 2024-02-12
# Description: Display error messages to the user on a per monitor basis
####

error_notice () {

  if [[ $1 == "active" ]]; then
    error_active=$((error_active+1));

    if [[ $SHOW_INACTIVE == 1 ]]; then
      echo "";
      log "Monitor $2 is inactive. No action taken.";

    elif [[ "$4" == "single" ]]; then
    echo "";
    log "Monitor $2 is inactive. No action taken.";

    fi

  elif [[ $1 == "api" ]]; then
    error_api=$((error_api+1));

    if [[ $SHOW_INACTIVE == 1 ]]; then
      echo "";
      log "$2 Monitor returned with an API error."
      log_api "$3";

    fi

  elif [[ $1 == "group" ]]; then
    error_group=$((error_group+1));

    if [[ $SHOW_INACTIVE == 1 ]]; then
      echo "";
      log "Monitor $2 was not skipped because it is a group."

    fi

  fi

}