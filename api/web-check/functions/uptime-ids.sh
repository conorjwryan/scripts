#!/bin/bash
# shellcheck disable=SC1091,SC2154
# Web Check Script
# Location: api/web-check/functions/uptime-ids.sh
# Version 1.1.0
# Author: Conor Ryan
# Date: 2024-02-12
# Description: Get BetterStack uptime monitor and heartbeat IDs
####

  uptime_ids () {
    echo "";
    log "Executing ID API Call:";
    echo "";

    if [ -z "${UPDATE_API}" ]; then
      echo "";
      log "No API supplied. update the .env file";
      exit 1
    fi

    log "Getting Monitor IDs"
    echo "";

    get_monitor_ids=$(curl -s --request GET --url https://uptime.betterstack.com/api/v2/monitors --header 'Authorization: Bearer '"${UPDATE_API}"''); 

    log_api "${get_monitor_ids}";

    echo -e "ID\tName";

    for k in $(jq ".data|keys|.[]" <<< "$get_monitor_ids"); do
      value=$(jq -r ".data[$k]" <<< "$get_monitor_ids");
      monitor_list_id=$(jq -r '.id' <<< "$value");
      monitor_list_name=$(jq -r '.attributes.pronounceable_name' <<< "$value");
      printf '%s\t%s\t\n' "$monitor_list_id" "$monitor_list_name";
    done | column -t -s$'\t'
    echo -e "\n"
    
    log "Getting Heartbeat IDs";
    echo "";

    get_heartbeat_ids=$(curl -s --request GET --url https://uptime.betterstack.com/api/v2/heartbeats --header 'Authorization: Bearer '"${UPDATE_API}"'');

    log_api "${get_heartbeat_ids}";

    echo -e "ID\tName";

    for k in $(jq ".data|keys|.[]" <<< "$get_heartbeat_ids"); do
      value=$(jq -r ".data[$k]" <<< "$get_heartbeat_ids");
      heartbeat_list_id=$(jq -r '.id' <<< "$value");
      heartbeat_list_name=$(jq -r '.attributes.name' <<< "$value");
      printf '%s\t%s\t\n' "$heartbeat_list_id" "$heartbeat_list_name";
    done | column -t -s$'\t'
    echo -e "\n"

    log "Getting Monitor Group IDs";
    echo "";

    get_group_ids=$(curl -s --request GET --url https://uptime.betterstack.com/api/v2/monitor-groups --header 'Authorization: Bearer '"${UPDATE_API}"'');

    log_api "${get_group_ids}";

    echo -e "ID\tName";

    for k in $(jq ".data|keys|.[]" <<< "$get_group_ids"); do
      value=$(jq -r ".data[$k]" <<< "$get_group_ids");
      group_list_id=$(jq -r '.id' <<< "$value");
      group_list_name=$(jq -r '.attributes.name' <<< "$value");
      printf '%s\t%s\t\n' "$group_list_id" "$group_list_name";
    done | column -t -s$'\t'
    echo -e "\n"

    exit 0;
    
  }