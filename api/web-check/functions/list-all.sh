#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2153
# List All Script
# Location: api/web-check/functions/list-all.sh
# Version 1.0.0
# Author: Conor Ryan
# Date: 2024-10-16
# Description: List monitors, heartbeats, and groups configured in the webcheck env file
####

list_all () {
  echo -e "Listing the current configuration of monitors, heartbeats, and groups in the .env file"

  echo -e "\nListing Monitors:"
  echo -e "ID\tName\tActive"

  index=0
  while [ -n "${MONITOR_ID[$index]}" ]; do
    monitor_id="${MONITOR_ID[$index]}"
    monitor_name="${MONITOR_LONGNAME[$index]}"
    monitor_active="${MONITOR_ACTIVE[$index]}"
    monitor_type="${MONITOR_TYPE[$index]}"
    active_status="No"
    if [ "$monitor_active" -eq 1 ]; then
      active_status="Yes"
    fi

    if [ "$monitor_type" == "monitor" ]; then
      printf '%s\t%s\t%s\t\n' "$monitor_id" "$monitor_name" "$active_status"
    fi
    index=$((index + 1))
  done | column -t -s$'\t'

  echo -e "\nListing Heartbeats:"
  echo -e "ID\tName\tActive"

  index=0
  while [ -n "${MONITOR_ID[$index]}" ]; do
    monitor_id="${MONITOR_ID[$index]}"
    monitor_name="${MONITOR_LONGNAME[$index]}"
    monitor_active="${MONITOR_ACTIVE[$index]}"
    monitor_type="${MONITOR_TYPE[$index]}"
    active_status="No"
    if [ "$monitor_active" -eq 1 ]; then
      active_status="Yes"
    fi

    if [ "$monitor_type" == "heartbeat" ]; then
      printf '%s\t%s\t%s\t\n' "$monitor_id" "$monitor_name" "$active_status"
    fi
    index=$((index + 1))
  done | column -t -s$'\t'

  echo -e "\nListing Groups:"

  echo -e "ID\tName\tActive"

  index=0
  while [ -n "${MONITOR_ID[$index]}" ]; do
    monitor_id="${MONITOR_ID[$index]}"
    monitor_name="${MONITOR_LONGNAME[$index]}"
    monitor_active="${MONITOR_ACTIVE[$index]}"
    monitor_type="${MONITOR_TYPE[$index]}"
    active_status="No"
    if [ "$monitor_active" -eq 1 ]; then
      active_status="Yes"
    fi

    if [ "$monitor_type" == "monitor-group" ]; then
      printf '%s\t%s\t%s\t\n' "$monitor_id" "$monitor_name" "$active_status"
    fi
    index=$((index + 1))
  done | column -t -s$'\t'
}