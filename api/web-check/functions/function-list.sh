#!/bin/bash
# shellcheck disable=SC1091,SC2154
# Web Check Script - Function List
# Location: api/web-check/functions/function-list.sh
# Version 1.1.0
# Author: Conor Ryan
# Date: 2024-10-16
# Description: List of functions used in the web-check script
####

# Logs Function
source ./functions/logs.sh

# Error Notice Function
source ./functions/error-notice.sh

# Uptime IDs Function
# Used to display the IDs of all monitors and heartbeats
source ./functions/uptime-ids.sh

# List Monitors Function
# Used to display all monitors and their current status
source ./functions/list-all.sh