#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2034
# Web Check Script
# Location: api/web-check/pre/script-mode.sh
# Version 2.0.0
# Author: Conor Ryan
# Date: 2024-10-16
# Description: Argument logic for the script to determine if the script should pause or unpause monitors
####

verbose="0"
help="0"
list="0"
config="0"

# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
  case $1 in
    --verbose|-v) verbose="1"; shift ;;
    --help|-h) help="1"; shift ;;
    --config|-c) config="1"; shift ;;
    --list|-l|--status¡-s) list="1"; shift ;;
    *) action="$1"; shift ;;
  esac
done

if [[ ${help} == "1" ]]; then
  echo "Usage: $0 [options] {pause|unpause} <monitor/heartbeat/group>"
  echo "Options:"
  echo "  --verbose, -v      Show API responses (diagnostic)"
  echo "  --help, -h         Show this help message"
  echo "  --config, -c       List IDs of monitors/heartbeats/groups to help configure .env"
  echo "  --list, -l,        List all monitors/heartbeats currently configured in .env"
  exit 0
fi

if [[ ${config} == "1" ]]; then
  uptime_ids;
  exit 0
fi

if [[ ${list} == "1" ]]; then
  list_all;
  exit 0
fi

if [[ $action == "pause" ]]; then
  pause_status="true"
  pause_word="Pausing"
  paused="Paused"
elif [[ $action == "unpause" ]]; then
  pause_status="false"
  pause_word="Unpausing"
  paused="Unpaused"
elif [[ $action == "ids" ]]; then
  uptime_ids;
  exit 0
else
  echo "Invalid Selection. You must either run 'pause', 'unpause' <monitor>"
  echo "List all .env configured monitors/heartbeats/groups you can interact with by adding --list"
  exit 1
fi

if [[ ${verbose} == "1" ]]; then
  echo "Verbose mode enabled"
fi