#!/bin/bash
# shellcheck disable=SC1091,SC2154
# Web Check Script
# Location: api/web-check/pre/script-checks.sh
# Version 1.0.4
# Author: Conor Ryan
# Date: 2024-02-12
# Description: Check for script pre-requisites before running the script
####

####
# Check for jq
####
# jq is used to parse the JSON output from the API calls
####

if ! command -v jq &> /dev/null; then
    log "jq could not be found. Please install jq and try again.";
    log "The script will now exit";
    exit 1;
fi

####
# Check for cURL
####
# cURL is used to make the API calls
####

if ! command -v curl &> /dev/null; then
    log "cURL could not be found. Please install cURL and try again.";
    log "The script will now exit";
    exit 1;

fi

####
# Check for API Key
####
# The API key is used to authenticate with the BetterStack API
####

if [ -z "${UPDATE_API}" ]; then
  log "No API supplied. update the .env file";
  exit 1
  
fi