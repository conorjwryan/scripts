# web-check.sh

Better Stack Uptime is a web monitoring platform that you can use to monitor websites and server cronjobs among [other things](https://betterstack.com/docs/uptime/uptime-monitor/). The idea being that when a website you're tracking goes down it alerts you so you can begin investigating any issues you have. One of the most attractive features for individuals and small companies is that they offer the ability to use 10 monitors for [free](https://betterstack.com/uptime/pricing)!

## Background

I personally use this service to track the uptime of websites that I manage as well as tracking that server tasks (cronjobs) executed properly. This works surprisingly well. However, as with any there are times you need to stop a monitor (such as when doing system upgrades). This can be achieved through the online portal but when you have many monitors it can be time consuming to click each one off (and back on again when finished).

Better Stack Uptime has expansive API documentation that can help us automate that process. Using the API I have devised a simple pause/unpause script for Better Stack Uptime monitors.

## Script Setup

This script comes with the ability to pause/unpause unlimited monitors/heartbeats/groups but they have to be configured in the `.env` file.

It is run by doing the following:

1. Ensure you have set up a Better Stack Uptime account and added at least 1 monitor. [(see here for an in depth tutorial on how)](https://betterstack.com/docs/uptime/monitoring-start/)

2. Copy the `sample.web-check.env` file to `.web-check.env`

    ```bash
    cp sample.web-check.env .web-check.env
    ```

3. Get your API key from the from Better Stack Uptime [HERE](https://betterstack.com/docs/uptime/api/getting-started-with-uptime-api/)

4. Put the API in the `UPDATE_API` variable

    Open the `.web-check.env` file with your favourite editor.

    ```bash
    nano .web-check.env 
    ```

    Input the API from `Step 3` in `UPDATE_API`

5. Make the script executable:

    ```bash
    sudo chmod +x web-check.sh
    ```

6. To find your monitor / heartbeat / group IDs by running the script

    ```bash
    ./web-check.sh --config
    ```

    Make a note of the IDs and their corresponding names to be used in `Step 7`

7. Edit and follow the steps to configure the `.web-check.env` file:

    ```bash
    nano .web-check.env
    ```

    Monitor are set up using the 5 variable arrays below:

    ```bash
    # "longname refers to the title of the website "
    MONITOR_LONGNAME[0]=""

    # "shortname is a one word phrase / nickname for ease of use"
    # "it'll be used in syntax to un/pause single website"
    MONITOR_SHORTNAME[0]=""

    # "id is unique ID for the monitor / heartbeat / group"
    # "required for the api call"
    # "can be found by running ./web-check.sh ids"
    MONITOR_ID[0]=""

    # "type is either monitor, monitor-group or heartbeat"
    # "required for the api call"
    MONITOR_TYPE[0]=""

    # "active is either 1 or 0"
    # "active=1 monitors will processed"
    # "active=0 monitors will be skipped"
    MONITOR_ACTIVE[0]=""
    ```

    **Note: Each group of arrays MUST have the same index number.**

    MONITOR_LONGNAME[0] = MONITOR_SHORTNAME[0] = MONITOR_ID[0] = MONITOR_TYPE[0] = MONITOR_ACTIVE[0]

    `0` is the index number. This is the number that is used to identify the monitor in the script.

    Add more monitors by incrementing the index number by 1 for each monitor you add.

    For example:

    ```bash
    MONITOR_LONGNAME[0]="My Website"
    MONITOR_SHORTNAME[0]="website"
    MONITOR_ID[0]="123456789"
    MONITOR_TYPE[0]="website"
    MONITOR_ACTIVE[0]="1"

    MONITOR_LONGNAME[1]="My Heartbeat"
    MONITOR_SHORTNAME[1]="heartbeat"
    MONITOR_ID[1]="987654321"
    MONITOR_TYPE[1]="heartbeat"
    MONITOR_ACTIVE[1]="0"

    MONITOR_LONGNAME[1]="My Monitor Group"
    MONITOR_SHORTNAME[1]="group"
    MONITOR_ID[1]="987654321"
    MONITOR_TYPE[1]="monitor-group"
    MONITOR_ACTIVE[1]="1"
    ```

    **Note: Only "active" monitors in the script will process. where `MONITOR_ACTIVE[x]=1` otherwise they will be skipped**

    In the above example, the script will process the "My Website" but will skip "My Heartbeat" as it is not active.

    **Note: Arrays begin with a 0 index `MONITOR_LONGNAME[0]` not `MONITOR_LONGNAME[1]`**

8. Run it with the following command:

    ```bash
    ./web-check.sh $1 $2 --verbose
    ```

    Where `$1` is either 'pause' or 'unpause'

    If `$2` is blank (in which case ALL configured monitors are paused/unpaused)

    Otherwise supply the monitor / monitor_group shortname you configured in the `.web-check.env` file

    **Note: Only "active" monitors in the script will process. where `MONITOR_ACTIVE=1` otherwise they will be skipped**

    **Note: "Monitor Groups" will not be paused if not paused individually**

## Script Options

```markdown
The script accepts the following options:

- `--verbose, -v`: Show API responses (diagnostic).
- `--help, -h`: Show the help message.
- `--config, -c`: List IDs of monitors/heartbeats/groups to help configure `.env`.
- `--list, -l,`: List all monitors/heartbeats currently configured in `.env`.

Usage:
```bash
./web-check.sh [options] {pause|unpause} <monitor/heartbeat/group>
```

### Logging and Debugging

The script provides several logging options that can be configured in the `.web-check.env` file:

- `LOG_LOCATION`: Specifies the directory where log files will be stored. Ensure the path ends with a `/`.
- `LOG_NAME`: Defines the name of the log file. By default, it uses the script name with a `.log` extension.
- `LOG_LEVEL`: Sets the level of logging detail. Options are:
  - `STANDARD`: Records basic information like monitor names and action success.
  - `EXTENDED`: Records API responses, outputting them to both the log and console.
- `LOG_OVERWRITE`: Determines if the log file should be overwritten each time the script runs. Set to `1` to overwrite, or `0` to append to the existing log file.

Example configuration in `.web-check.env`:

```dotenv
LOG_LOCATION="../../logs/"
LOG_NAME="${script_name}.log"
LOG_LEVEL="STANDARD"
LOG_OVERWRITE=1
```

To enable detailed logging, set `LOG_LEVEL` to `EXTENDED` and ensure `LOG_LOCATION` and `LOG_NAME` are correctly configured. This will help in debugging by providing comprehensive logs of the script's operations and API interactions.
