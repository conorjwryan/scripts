#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2034
# Web Check Script
# Location: api/web-check/main/monitor-logic.sh
# Version 1.1.0
# Author: Conor Ryan
# Date: 2023-02-12
# Description: Logic for pausing/unpausing specific monitors or all monitors
####

# TODO: Switch this into a function
# START logic apply to all monitors / heartbeats
if [[ -z "$2" || "$2" == "all" ]]; then
  echo "";
  log "$pause_word All Monitors and Heartbeats";

  for i in "${!MONITOR_LONGNAME[@]}";
    do
      if [ "${MONITOR_ACTIVE[$i]}" == 0 ]; then
        error_notice "active" "$i";
        continue
      
      elif [ "${MONITOR_TYPE[$i]}" == "monitor-group" ]; then
        error_notice "group" "$i";
        continue;

      fi

      echo ""
      log "$pause_word ${MONITOR_LONGNAME[$i]}";

      get_monitor_check=$(curl -s --request PATCH --url https://uptime.betterstack.com/api/v2/"${MONITOR_TYPE[$i]}"s/"${MONITOR_ID[$i]}" --header 'Authorization: Bearer '"${UPDATE_API}"'' --header 'Content-Type: application/json' --data '{ "paused": '"${pause_status}"' }') 

      get_monitor_api_response=$(echo "$get_monitor_check" | json_pp);

      log_api "${get_monitor_api_response}";
      
      monitor_api_error=$(jq 'has("errors")' <<< "$get_monitor_api_response");

      if [[ $monitor_api_error == "false" ]]; then
        log "Monitor ${MONITOR_LONGNAME[$i]} $paused";
      else
        log "Error pausing monitor ${MONITOR_LONGNAME[$i]}";
        error_notice "api" "${MONITOR_LONGNAME[$i]}" "${get_monitor_api_response}";
      fi

      unset monitor_api_error;
      unset get_monitor_api_response;
      unset get_monitor_api_output;

    done

  if [[ $SHOW_INACTIVE == 0 ]]; then

    if [[ $error_active -gt 0 ]]; then
      echo "";
      log "$error_active monitor(s) skipped.";
    fi

    if [[ $error_api  -gt 0 ]]; then
      log "${error_api} monitor(s) with API errors.";
      echo "";
      echo "";
      log "Not All monitors ${paused}";
    
    else
      echo "";
      log "All monitors ${paused}";

    fi

    if [[ $error_active -gt 0 ]] || [[ $error_api -gt 0 ]]; then
      echo "";
      log "For more information set 'SHOW_INACTIVE=1' in the scripts .env file ";

    fi

  fi

  exit 0;

# END apply to all monitors / heartbeats / groups
else

  # This part of the script will check if $2 is a valid monitor or heartbeat nickname
  # If $2 is not a valid monitor or heartbeat nickname, the script will exit with an error
  # If $2 is a valid monitor or heartbeat nickname, the script will pause or unpause that monitor or heartbeat logic below
  

  # check if monitor nickname ($2) is in the env array
  # j = index of $2 in the monitor array
  index=-1;
  for j in "${!MONITOR_SHORTNAME[@]}"; do
    if [[ "${MONITOR_SHORTNAME[j]}" == "$2" ]]; then

      single_monitor_id=${MONITOR_ID[$j]};
      single_monitor_longname=${MONITOR_LONGNAME[$j]};
      single_monitor_type=${MONITOR_TYPE[$j]};
      single_monitor_active=${MONITOR_ACTIVE[$j]};
      
      index=$j;
      break
    
    fi
 
  done

  # if $2 is not in the array, exit with an error
  if [[ $index -eq -1 ]]; then
    log "'$2' is not a valid monitor nickname. Please check the name and try again.";
    exit 1;

  fi

  # if $2 is in the array, check if it is active
  if [[ $single_monitor_active -eq 0 ]]; then
    error_notice "active" "$2" "$j" "single";

    exit 1;
  fi

  log "$pause_word $single_monitor_longname";
        
  single_monitor_check=$(curl -s --request PATCH --url https://uptime.betterstack.com/api/v2/"${single_monitor_type}"s/"${single_monitor_id}" --header 'Authorization: Bearer '"${UPDATE_API}"'' --header 'Content-Type: application/json' --data '{ "paused": '"${pause_status}"' }');

  single_monitor_api_response=$(echo "$single_monitor_check" | json_pp);

  log_api "${single_monitor_api_response}";
      
  single_monitor_api_error=$(jq 'has("errors")' <<< "$single_monitor_api_response");

  if [[ $single_monitor_api_error == "false" ]]; then
  log "Monitor ${single_monitor_longname} $paused";
  else
    log "Error pausing monitor ${single_monitor_longname}";
    error_notice "api" "${single_monitor_longname[$j]}" "${single_monitor_api_response}";
  fi

  unset single_monitor_api_error;
  unset single_monitor_api_response;
  unset single_monitor_api_output;  

fi