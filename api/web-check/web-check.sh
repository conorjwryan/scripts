#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2034
# Web Check Script
# Location: api/web-check/web-check.sh
# Version 1.1.2
# Author: Conor Ryan
# Date: 2024-02-05
# Description: Pause or unpause BetterStack uptime monitors and heartbeats
####

script_name="web-check"

# Change Directory to Script Directory
cd "$(dirname "$0")" || exit 1

# Load .env file
if [ ! -f ".web-check.env" ]; then
  echo -e "No '.web-check.env' file found"
  echo -e "Make sure that you have copied from the sample file into the script directory first before executing this script"
  exit 1

fi

set -o allexport
source .web-check.env
set +o allexport

# Set Error Handling
error_active=0;
error_api=0;

# Add functions
source ./functions/function-list.sh

# Script Prerequisites
source ./pre/script-checks.sh

# Script Mode Selection
source ./pre/script-mode.sh

####
# Monitor ID Script Logic
# The logic for the script is contained in this part of the script based on $2
# 1. If $2 is empty or 'all', the script will pause or unpause all monitors and heartbeats
# 2. If $2 is a valid monitor or heartbeat nickname, the script will pause or unpause that monitor or heartbeat
source ./main/monitor-logic.sh

# END SCRIPT
exit 0;
