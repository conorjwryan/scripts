# ntfy-curl.sh

This script is used to send notifications using the NTFY service. Useful if you want to send a notification from a script but can't install NTFY or configure Postfix on the machine.

## How to Setup the Script

The following instructions detail the required software, information, and setup required to use this script.

It can be run on both Linux and macOS. For Mac, you will need to install the required software using [Homebrew](https://brew.sh/).

### Required Software

- `cURL` - this script uses `cURL` to send notifications to NTFY. This is installed by default on most `Linux` distributions and `macOS`. If you do not have it installed, you can install it using your `package manager`.

- `ntfy server` - this script requires the NTFY server to be running somewhere in your infrastructure. This is easily done by running the NTFY server in a Docker container. You can find the instructions for setting up the NTFY server [here](https://docs.ntfy.sh/install/).

- `email provider` (optional) - if you want to send email notifications, you will need to have an email provider set up. This can be a local mail server or a third-party provider like `SendGrid` or `Mailgun` and then the email settings will need to be configured in the NTFY server.

If the above software is not installed the script will not run.

### Required Information

In order to run this script, you will need to gather the following information from your NTFY installation:

- `NTFY_TOKEN` - the token for authenticating with the NTFY service.
- `NTFY_URL` - the URL of the NTFY service.
- `NTFY_TOPIC` - the topic to which the notification will be sent.
- `NTFY_EMAIL` (optional) - the email address to send the notification to if email notifications are enabled.

### Script Setup and Configuration

1. Clone this repository to your local machine and run the script from the command line.

    ```bash
    git clone https://gitlab.com/conorjwryan/scripts.git
    ```

    Enter the `ntfy-curl` directory.

    ```bash
    cd scripts/api/ntfy-curl
    ```

2. Create the `.env` file from the sample file `sample.ntfy-curl.env` and open it in your preferred text editor.

    ```bash
    cp sample.ntfy-curl.env .ntfy-curl.env
    ```

    ```bash
    nano .ntfy-curl.env
    ```

3. Based on the information gathered from "[Required Information](#required-information)" step above fill in the required information in the `.env` file.

    The `.env` file contains the following variables:

    - `NTFY_TOKEN` - the token for authenticating with the NTFY service.
    - `NTFY_URL` - the URL of the NTFY service.
    - `NTFY_TOPIC` - the topic to which the notification will be sent.
    - `NTFY_EMAIL` (optional) - the email address to send the notification to if email notifications are enabled.

    Once you are done save and close the file.

4. The script should be executable by default but if it is not, make it executable:

    ```bash
    chmod +x ntfy-curl.sh
    ```

5. You can now run the script and see the envs are loaded correctly and send a test message.

    ```bash
    ./ntfy-curl.sh --env

    # Output
    # NTFY_TOKEN=<your_ntfy_token>
    # NTFY_URL=<your_ntfy_url>
    # NTFY_TOPIC=<your_ntfy_topic>
    # NTFY_EMAIL=<your_ntfy_email>

    ./ntfy-curl.sh --test --verbose --email
    # Output
    # Sending test notification to NTFY server...
    ```

    A test notification will be sent to the NTFY server and if email is enabled, an email will be sent as well.

6. You can now use the script to send notifications.

    ```bash
    ./ntfy-curl.sh --title="Test Title" --message="Test Message"
    ```

## How to Use the Script

To use this script, simply run it from the command line with the required parameters.

```bash
./ntfy-curl.sh --title="<title>" [--message="<message>"] [--verbose] [--email] [--env] [--test] [--help] 
```

### Parameters

- `--title` (required) - the title of the notification.
- `--message` (optional) - the message of the notification. If not provided, the title will be used as the message.
- `--verbose` (optional) - enable verbose mode to show the cURL request.
- `--email` (optional) - send email notification as well (has to be enabled in NTFY).
- `--env` (special) - show the environment variables.
- `--test` (special) - send a test notification.
- `--help` (special) - show usage information.

### Examples

Send a notification with a title and message:

```bash
./ntfy-curl.sh --title="Test Title" --message="Test Message"
```

Send a notification with a title only:

```bash
./ntfy-curl.sh --title="Test Title"
```

Send a notification with verbose mode enabled:

```bash
./ntfy-curl.sh --title="Test Title" --verbose
```

Send a notification with email notification enabled:

```bash
./ntfy-curl.sh --title="Test Title" --email
```

## Optional Extra Steps

### Referencing NTFY-CURL from Anywhere on the System

If you want to be able to run either this script interactively, via an automated cronjob or from other scripts on your system it is advisable to add add `$NTFY_CURL_HOME` to the `/etc/environment` file.

**Note:** You will need to have administrator (`sudo`) privileges to do this.

```bash
echo 'NTFY_CURL_DIR=/path/to/ntfy-curl/directory' | sudo tee -a /etc/environment
```

This will add the `NTFY_CURL_DIR` environment variable to the `/etc/environment` file. A reboot is required before the changes take effect.

 You can then reference this variable in other scripts or cronjobs as needed. To test that the variable is set correctly, you can run the following command:

```bash
$NTFY_CURL_DIR/ntfy-curl.sh --test
```

## Future Improvements

- Better Error Handling - currently even if the cURL request fails the script will not provide detailed error messages. This will be fixed in future.
- More Notification Options - currently, the script only allows for sending a title and message. Future improvements will include more options for sending notifications.
- Implement into Other Scripts - this script will be implemented into other scripts in this repository to allow for notifications to be sent from those scripts.
