#!/bin/bash
# shellcheck disable=SC1091,SC2034,SC2154
# NTFY cURL Script - Main Script
# Location: api/ntfy-curl/ntfy-curl.sh
# Version 1.1.1
# Author: Conor Ryan
# Date: 2024-10-10
# Description: Script to send cURL requests to NTFY Notification Service
####

script_name="ntfy-curl"
verbose="0"
env="0"
test="0"
help="0"
email="0"
title=""
message=""

# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
  case $1 in
    --verbose|-v) verbose="1"; shift ;;
    --email|-e) email="1"; shift ;;
    --title=*) title="${1#*=}"; shift ;;
    --message=*) message="${1#*=}"; shift ;;
    --env) env="1"; shift ;;
    --test) test="1"; shift ;;
    --help) help="1"; shift ;;
    *) echo "Unknown parameter passed: $1"; exit 1 ;;
  esac
done

if [[ $help == "1" ]]; then
  echo -e "Usage: ./${script_name}.sh --title=\"<title>\" [--message=\"<message>\"]"
  echo -e "Only Title is required, Message is optional"
  echo -e "Example: ./${script_name}.sh --title=\"Test Title\" --message=\"Test Message\""
  echo -e "";
  echo -e "Use the --env flag to show the environment variables"
  echo -e "Use the --test flag to send a test notification"
  echo -e "Use the --verbose (-v) flag to more information"
  echo -e "Use the --email (-e) flag to send email notification as well (has to be enabled in NTFY)"
  exit 0
fi

# Get the directory of the script
if [[ -z ${NTFY_CURL_DIR} ]]; then
  NTFY_CURL_DIR=$(dirname "$0")

  if [[ $verbose == "1" ]]; then
    echo -e "NTFY_CURL_DIR not previously defined"
    echo -e "Setting NTFY_CURL_DIR to ${NTFY_CURL_DIR}"
  fi

fi

# Load .env file
if [ ! -f "${NTFY_CURL_DIR}/.ntfy-curl.env" ]; then
  echo -e "No '.ntfy-curl.env' file found"
  echo -e "Make sure that you have copied from the sample file into the script directory first before executing this script"
  exit 1

fi

# Export environment variables from the .env file
set -o allexport
source ${NTFY_CURL_DIR}/.ntfy-curl.env
set +o allexport

# Check if .ntfy-curl.env VARIABLES are not empty
if [ -z "${NTFY_TOKEN}" ] || [ -z "${NTFY_URL}" ] || [ -z "${NTFY_TOPIC}" ]; then
  echo -e "One or more variables are undefined in the '.ntfy-curl.env' file"
  exit 1
fi

# Check if email is enabled
if [[ $email == "1" ]]; then
  if [ -z "${NTFY_EMAIL}" ]; then
    echo -e "Email is not defined in the '.ntfy-curl.env' file"
    exit 1
  fi
fi

# Add option to see the environment variables
if [[ $env == "1" ]]; then
  echo -e "NTFY cURL Script Environment Variables"
  echo -e "--------------------------------"
  echo -e "NTFY_CURL_DIR: ${NTFY_CURL_DIR}"
  echo -e "NTFY Token: ${NTFY_TOKEN}"
  echo -e "NTFY URL: ${NTFY_URL}"
  echo -e "NTFY Topic: ${NTFY_TOPIC}"
  echo -e "NTFY Email: ${NTFY_EMAIL}"
  exit 0
fi

# Test Mode - Send cURL Request to NTFY Notification Service
if [[ $test = '1' ]]; then
  echo "Sending Test Notification to NTFY Notification Service";
  title="Test Notification"
  message="This is a test notification from NTFY-cURL Script"
fi

# Make sure title is provided
if [ -z "$title" ]; then
  echo -e "Title is required";
  echo -e "Usage: ./${script_name}.sh --title=\"<title>\""
  echo -e "";
  echo -e "Use the --help for more infomation"
  exit 1
fi

# If message is not provided, use title as message
if [ -z "$message" ]; then
  message="${title}"
fi

full_path="${NTFY_URL}${NTFY_TOPIC}"

# Send cURL Request to NTFY Notification Service
if [ ${verbose} == "0" ]; then
  if [[ $email == "1" ]]; then
    curl ${full_path} \
        -H "Authorization: Bearer ${NTFY_TOKEN}" \
        -H "Email: ${NTFY_EMAIL}" \
        -H "Title: ${title}" \
        -d "${message}" > /dev/null 2>&1
  else
    curl ${full_path} \
        -H "Authorization: Bearer ${NTFY_TOKEN}" \
        -H "Title: ${title}" \
        -d "${message}" > /dev/null 2>&1
  fi

else
  echo -e "Title: ${title}"
  echo -e "Message: ${message}"
  echo -e "URL: ${full_path}"
  if [[ $email == "1" ]]; then
    echo -e "Email: ${NTFY_EMAIL}"
  fi
  echo -e "\nSending cURL Request to NTFY Notification Service"

  if [[ $email == "1" ]]; then
    curl ${full_path} \
      -H "Authorization: Bearer ${NTFY_TOKEN}" \
      -H "Email: ${NTFY_EMAIL}" \
      -H "Title: ${title}" \
      -d "${message}"
  else
    curl ${full_path} \
      -H "Authorization: Bearer ${NTFY_TOKEN}" \
      -H "Title: ${title}" \
      -d "${message}"
  fi
fi