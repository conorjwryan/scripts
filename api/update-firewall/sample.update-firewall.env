############################################
# UPDATE FIREWALL ENVIRONMENT VARIABLES
############################################
# This file is used to store the environment variables for the Update-firewall API Script.
# See the README.md file for more information on how to configure this file.
############################################

############################################
# LOGGING SETTINGS
############################################
# 'LOG_LOCATION' is the location of the log file either relative or absolute path (it must end with a /) 
LOG_LOCATION="../../logs/"

# 'LOG_NAME' is the name of the log file
# The default is "${script_name}.log"
# If you wanted to set the file to the date/time:
# LOG_NAME="${script_name}_$(date +'%Y-%m-%d_%H-%M-%S').log"
# You can change this to whatever you want
LOG_NAME="${script_name}.log"

# 'LOG_LEVEL' is the level of logging you want to use
# Options are:
# 'STANDARD' - basic logging including errors
# 'VERBOSE' - will show more information (output to terminal and log)
# 'EXTENDED' -  will record the API response as well (output to terminal and log)
LOG_LEVEL="STANDARD"

# 'LOG_OVERWRITE' is a flag to determine if the log file should be overwritten each time the script is run
# The default is 0 - which means the log file will not be overwritten but appended to
# If you want to overwrite the log file each time the script is run, set this to 1
LOG_OVERWRITE=1

############################################
# GENERAL SCRIPT SETTINGS
############################################

# THIS WILL CHANGE AS THE SCRIPT IS RUN
CURRENT_IP="";

# Input the current name of the system this is running on. 
HOSTNAME="";

# The terminal command sed is used in this script to update this env file with the updated IP address
# Linux and macOS use different versions of sed meaning in order to run this script on both systems without changing the code please input the correct sed command below
# Linux uses 'sed' and macOS uses 'gsed'
# If you are using Linux you can leave it unchanged
# If you are using macOS you will need to install gsed using homebrew
# brew install gnu-sed
# Then change the command below to 'gsed'
SCOMMAND="sed";

############################################
# NOTIFICATION SETTINGS
############################################

# Enable or disable email notifications if the IP address has changed
# EMAIL_ACTIVE=0; means the email function will be skipped
# EMAIL_ACTIVE=1 means the email function will run
EMAIL_ACTIVE=0;

# This is the email service you want to use for notifications
# To use Sendgrid set this to 'sendgrid'
# To use Mail set this to 'mail'
# To use ntfy-curl set this to 'ntfy-curl'
# To use a custom command set this to 'custom'
EMAIL_COMMAND="";

# This is the API key for the email service Sendgrid
# If using Sendgrid get your api key https://sendgrid.com/docs/ui/account-and-settings/api-keys/
SENDGRID_API="";

# This is the custom command you want to use for (email) notifications
# This is only to be used if you have chosen the 'custom' command
# The command must be a valid terminal command
# You can use the variables below to pass in the data from this script like $CURRENT_IP or $EMAIL_TO
CUSTOM_CMD="";
CUSTOM_CMD_NAME="";

# This is the email address you want to send the notification from
# This must be a verified email address from a verified domain
EMAIL_FROM="";

# This is the email address you want to send the notification to
EMAIL_TO="";

# This is the subject of the email
EMAIL_SUBJECT="IP on '$HOSTNAME' has been updated";

# This is the body of the email
# Only to be used if you have chosen the 'mail' command
EMAIL_BODY="";

# Wait time in seconds before sending the notification
# While it might only take a few seconds for the IP to update it is best to wait a exitra seconds before sending the notification otherwise the email might be sent before the IP has updated
# Default is 30 seconds
WAIT_TIME=30;

############################################
# API SETTINGS
############################################

####
## Digital Ocean API (service code 'do')
####
# this script does not create a new firewall but rather updates one that already exists
# go here learn how to create one https://docs.digitalocean.com/products/networking/firewalls/quickstart/

####
## Cloudflare API (service code 'cf')
####
# this script assumes you have already set up Cloudflare Zero Trust Networking and have access control lists ACLs in place
# to learn more about Cloudflare Zero Trust go here: 
# https://www.cloudflare.com/en-gb/learning/security/glossary/what-is-zero-trust/
# 

############################################
# FIREWALL SETTINGS
############################################

# FIREWALL_NAME_1="Company Digital Ocean Firewall";
# This is a descriptive name for this script eg:
# ---
# FIREWALL_ACTIVE_1=1;
# This is a flag to determine if the firewall should be updated
# 1 means the firewall will be updated
# 0 means the firewall will not be updated
# ---
# FIREWALL_SERVICE_1="cf";
# This is the service you are using for the firewall
# Currently only Digital Ocean - "do" and Cloudflare "cf" are supported
# ---
# FIREWALL_API_TOKEN_1="your_api_token";
# This is the API token for the firewall service
# ---
# FIREWALL_ACCOUNT_ID_1="your_account_id";
# This is the account ID for the firewall service
# This is only required for Cloudflare not Digital Ocean
# ---
# FIREWALL_GROUP_ID_1="your_firewall_id";
# The only way to find your firewall ID is to do an API call
# Running './update-firewall.sh --list' will list of firewalls IDs and Names on your account
# REQUIRES API TOKEN TO BE INPUT INTO 'FIREWALL_API_TOKEN_1' ABOVE
# ---
# FIREWALL_GROUP_NAME_1="your_firewall_name";
# Refers to the name you have given the firewall
# Running './update-firewall.sh list-firewalls' will list of firewalls IDs and Names on your account
# Use the name as it appears in the list

# To add another firewall just increment the number by 1
# FIREWALL_NAME_2="Another Firewall";
# FIREWALL_ACTIVE_2=1;
# FIREWALL_SERVICE_2="cloudflare";
# FIREWALL_API_TOKEN_2="your_api_token";
# FIREWALL_ACCOUNT_ID_2="your_account_id";
# FIREWALL_GROUP_ID_2="your_firewall_id";
# FIREWALL_GROUP_NAME_2="your_firewall_name";

