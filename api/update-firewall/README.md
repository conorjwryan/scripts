# update-firewall.sh

This script is designed to update the IP address of a firewall or Access Control List (ACL) on `Digital Ocean` or `Cloudflare` respectively. This is useful if you have a dynamic IP address and need to update the firewall or ACL to allow access to your server.

## Background (Dynamically Allocated IP Addresses and Home Servers)

I run a mix of internally and externally hosted servers across my infrastructure for work and personal use. I have this all bound up in Cloudflare Zero Trust where through a mix of Access Control Lists I can manage who can use externally facing systems and (more crucially) from WHERE.

Now this in and of itself is not an issue. I have configured servers so that they can only be accessed through `SSH` / `SFTP` from my home (work) network, through a specific `IP Address` `123.45.6.78`. Ideally there is no issue, I "set it and forget it" but no because, my IP is dynamically allocated so every 30 days (or maybe even when the router turns off out of nowhere) I get a completely new IP `245.67.8.90` and this breaks my access to my infrastructure. Admittedly in my case this more of an annoyance than anything and is an easy fix but I don't want to do it manually so that's where this script comes in...

I have a `Raspberry Pi 4B` which sits on my network and acts as a Cloudflare Access Tunnel to the outside world. THe actual tunnel maintains it's IP internally but the `ACL groups` which I've setup to bypass external checks for my internal systems do not update properly. Likewise, the firewall I have setup on my Digital Ocean Droplets break my access also. I have set this script to run automatically every hour (cronjob) to update the firewalls/ACLs with my IP address.

## Considerations

1. This script assumes that you have a Digital Ocean firewall for a "droplet" (paid for) and/or an Access Control List for Cloudflare's "Zero Trust" offering (which is free).

    If you only have one and not the other. Set `DO_ACTIVE` or `CF_ACTIVE` to `0` in the `.firewall.env` file.

2. This script takes the IPv4 from the host or you can supply your own:

   In future I hope to add ability to add an IPv6 and multiple supplied IP addresses but for now only IPv4 works

3. This script is limited where it can currently only update one account for Digital Ocean and Cloudflare respectively.

    In future I hope to add the ability to add the ability to multiple accounts/`APIs` for both `Digital Ocean` and `CloudFlare`.

## Script Requirements

### cURL

This script uses `curl` to make the API calls to `Digital Ocean` and `Cloudflare`. If you do not have `curl` installed on your system you can install it with the following command:

```bash
sudo apt install curl # for Debian/Ubuntu
# or
sudo yum install curl # for CentOS/RHEL
# or
brew install curl # for MacOS
```

### jq

This script uses `jq` to parse the JSON responses from the API calls.

If you do not have `jq` installed on your system you can install it with the following command:

```bash
sudo apt install jq # for Debian/Ubuntu
# or
sudo yum install jq # for CentOS/RHEL
    # or 
brew install jq # for MacOS
```

### sed

When the script is run it saves the IP in the env file using the sed command.

MacOS does not have the same version of sed as Linux so if you are running this on a Mac you will need to install `gnu-sed` with the following command:

```bash
brew install gnu-sed
```

Then you will need to change update 'SCOMMAND' in the env file to the following:

```bash
sed_command="gsed"
```

## Script Setup

Clone the repository to your machine if you haven't already.

```bash
git clone https://gitlab.com/conorjwryan/scripts.git
```

cd into the `update-firewall` directory

```bash
cd scripts/api/update-firewall
```

Follow the steps below to gather your API Tokens and set up the script.

### Gathering API Tokens

To use this script you will need to get an API Token for Digital Ocean and/or Cloudflare.

#### Digital Ocean

For Digital Ocean go [here](https://docs.digitalocean.com/reference/api/create-personal-access-token/) to get an all encompassing API Token.

Note: They have recently announced scoped tokens which are more secure and can be used for specific tasks. This `README.md` will be updated to reflect this in the future.

#### Cloudflare

For Cloudflare things are a bit more involved and require 2 things:

- Your Account ID [found here](https://developers.cloudflare.com/fundamentals/get-started/basic-tasks/find-account-and-zone-ids/)
- Your Access Token can be generated following the steps [here](https://developers.cloudflare.com/fundamentals/api/get-started/create-token/). Cloudflare Access Tokens offer improved security and accountability due to the granular nature where API tokens can be created for a specific purpose. To that end when you have got to the API Token creation page follow these settings:

![Cloudflare API Options for Script](https://cdn.cjri.uk/scripts/static/images/api-readme-1.jpg)

### Setting up the Environment Variables

1. With the gathered tokens and keys now we need to input them into the `.env file`

    Make a copy of the sample `.update-firewall.env`file

    ```bash
    cp sample.update-firewall.env .update-firewall.env
    ```

2. Edit the newly created `.update-firewall.env` and add the information into file

    ```bash
    nano .update-firewall.env
    ```

    When first adding a new firewall you only need to change the following variables:

    For Cloudflare

    ```bash
    FIREWALL_NAME_1="Company Cloudflare Firewall";
    FIREWALL_ACTIVE_1=1;
    FIREWALL_SERVICE_1="CF";
    FIREWALL_ACCOUNT_ID_1="";
    FIREWALL_API_TOKEN_1="";
    ```

    For Digital Ocean:

    ```bash
    FIREWALL_NAME_2="Client Digital Ocean Firewall";
    FIREWALL_ACTIVE_2=1;

    FIREWALL_SERVICE_2="DO";
    FIREWALL_API_TOKEN_2="";
    ```

    There is no need for the `FIREWALL_ACCOUNT_ID` for Digital Ocean.

    NOTE: If you want to add more than one firewall follow the steps above and change the end indicator to the next number. This script allows for an unlimited number of firewalls.

Leave the rest unedited for now as you will need to gather the firewall/ACL ids and add them to the file later.

### Gather your Firewall/ACL IDs

If you have already have your firewall names and IDs firewalls/ACLs you can skip this section and go to [Running the script](#running-the-script). Otherwise do the following:

1. Run the following command to get a the ids of the firewalls and ACLs on one or both of your accounts.

    ```bash
    ./update-firewall.sh --list
    ```

    It will take the API Tokens supplied and come back with a list of firewalls and their associated names.

2. With the new firewall/ACL names and ids re-edit the `.update-firewall.env file`

    ```bash
    nano .update-firewall.env
    ```

    Add the ids to the respective firewall like so:

    ```bash
    FIREWALL_GROUP_ID_1="12345678-1234-1234-1234-1234567890";
    FIREWALL_GROUP_NAME_1="Company IP Group";

    FIREWALL_GROUP_ID_2="12345678-1234-1234-1234-1234567890";
    FIREWALL_GROUP_NAME_2="Client IP Group";
    ```

    Your `.update-firewall.env` file should now look something like this:

    ```bash
    FIREWALL_NAME_1="Company Cloudflare Firewall";
    FIREWALL_ACTIVE_1=1;
    FIREWALL_SERVICE_1="CF";
    FIREWALL_ACCOUNT_ID_1="12345678-1234-1234-1234-1234567890";
    FIREWALL_API_TOKEN_1="cf.12345678-1234-1234-1234-1234567890";
    FIREWALL_GROUP_ID_1="12345678-1234-1234-1234-1234567890";
    FIREWALL_GROUP_NAME_1="Company IP Group";

    FIREWALL_NAME_2="Client Digital Ocean Firewall";
    FIREWALL_ACTIVE_2=1;
    FIREWALL_SERVICE_2="DO";
    FIREWALL_API_TOKEN_2="12345678-1234-1234-1234-1234567890";
    FIREWALL_GROUP_ID_2="12345678-123";
    FIREWALL_GROUP_NAME_2="Client IP Group";
    ```

    Now that the script is set up you can either go to [Running the script](#first-run) or go to the [Email Section](#sending-email-notifications) to find out how to set up email notifications using `SendGrid`, `NTFY-cURL`, `mail` or your own custom solution.

## Running the Script

### First Run

Run the script with the `--verbose-api` flag to see the output of the script and the API calls.

```bash
./update-firewall.sh --verbose-api
```

### Subsequent Runs

Run the script without any `arguments` and your firewall/ACL should be updated using your host's external IPv4 with a CIDR of `/32`.

```bash
./update-firewall.sh 
```

With this script you can also supply your own IPv4 running it like:

```bash
./update-firewall.sh 192.168.0.1
```

or with `CIDR` notation:

```bash
./update-firewall.sh 10.0.0.0/8
```

## Script Options and Usage

Below are some relevant options and usage for the script including how to set up a `cronjob` to run the script automatically and how to send `IP` update email notifications.

The script has the following options:

- `--verbose, -v`: Enable verbose mode
- `--verbose-api, -va`: Enable verbose mode for API calls
- `--no-output` : Disable output to terminal except for errors

- `--test, -t`: Send Test notification/email
- `--list-firewalls, --list, -l`: List all firewalls

- `--help, -h`: Display this help message

### Examples

```bash
./update-firewall.sh (Auto-detect IP Address and run script)
./update-firewall.sh --no-output (Auto-detect IP Address and run script without output except for errors)

# Special Options
./update-firewall.sh --verbose 1.2.3.4 (Enables verbose mode and sets IP Address to 1.2.3.4)
./update-firewall.sh -l (Shows all firewalls)
```

## Sending Email Notifications

This script can send an email to you when the IP address is updated. It can be configured to use `Sendgrid`, `mail` or `ntfy-curl` to send the email as standard however you can also use a custom solution. See below for more information.

Based on your choice you will need to set the following variables in the `.update-firewall.env` file:

```bash
EMAIL_ACTIVE=1;

# To use Sendgrid set this to 'sendgrid'
# To use Mail set this to 'mail'
# To use ntfy-curl set this to 'ntfy-curl'
# To use a custom command set this to 'custom'
EMAIL_COMMAND="";

# this is the email address you want to send the notification from
# eg: server@example.com
EMAIL_FROM="server@example.com"

# this is the email address you want to send the notification to
EMAIL_TO="conor@example.com"

# this is the subject of the email
EMAIL_SUBJECT="IP on '$HOSTNAME' has been updated"

# this is the body of the email
# only to be used if you have chosen the 'mail' command
EMAIL_BODY=""
```

### SendGrid

SendGrid can be used as a SMTP Relay and is free for up to 100 emails a day. To use this you need to create an account [here](https://sendgrid.com/). Follow this [guide](https://docs.sendgrid.com/for-developers/sending-email/api-getting-started#prerequisites-for-sending-your-first-email-with-the-sendgrid-api) to get your SMTP credentials. Remember to authenticate your domain so that you can send emails from your domain.

Once you have your SMTP credentials add them to the `.update-firewall.env` file and follow the stated instructions.

### Mail

To setup mail you need to have `mail` installed on your system. This is usually installed by default on most Linux distributions but requires you to set it up server side. To do this follow the instructions [here](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-postfix-as-a-send-only-smtp-server-on-ubuntu-22-04).

Once you have your SMTP credentials add them to the `.update-firewall.env` file and follow the stated instructions.

#### Notes on 'Mail' Command

The `mail` command is used to send emails from the terminal. The command is used in the following way in the script:

```bash
echo "${EMAIL_BODY}" | "${EMAIL_COMMAND}" -s "${EMAIL_SUBJECT}" -aFrom:"${EMAIL_FROM}" "${EMAIL_TO}";
```

Make sure to edit the environment variables in the `.update-firewall.env` file to match your SMTP server settings.

### NTFY-cURL

`ntfy-curl` is a script that uses `curl` to send notifications to a self-hosted `ntfy` server. This is useful if you want to send a notification from a script but can't install the `ntfy` client or configure `Postfix` on the machine.

More info on `ntfy-curl` can be found [here](../ntfy-curl/README.md). If you would like to find out more about `ntfy` you can do so [here](https://ntfy.sh/).

### Custom Notification Command

If you would like to use a custom command to send an email you can do so by setting the `EMAIL_COMMAND` variable to `custom`. You will then need to set the `CUSTOM_CMD` variable to the command you would like to use. This command will be run as if directly from the terminal.

```bash
EMAIL_COMMAND="custom";
CUSTOM_CMD_NAME="Custom Command";
CUSTOM_CMD="echo '$EMAIL_BODY' | sendmail -s '$EMAIL_SUBJECT' -aFrom:'$EMAIL_FROM' $EMAIL_TO";
```

### Notification Wait Time

The script has a built-in "wait time" of 30 seconds to allow the `firewall/ACL` to update before sending the notification. This can be changed in the `.update-firewall.env` file.

```bash
WAIT_TIME=30;
```

### Testing Email Notifications

To test the email notifications you can run the script with the `--test` flag.

```bash
./update-firewall.sh --test
```

This will send a test email/notification to through the configured method above.

## Running this script as a cronjob

Run the script as a cronjob to update the firewall/ACL every hour.

```bash
crontab -e
```

Add the following line to the crontab file:

```bash
0 * * * * /path/to/update-firewall.sh --no-output --verbose
```

Save and exit the crontab file.

This will run the script every hour on the hour.

Dependant on your needs it could be worth only running it once a week in which case you would change the `0` to `0 0 * * 0` which would run the script at midnight on a Sunday.

```bash
0 0 * * 0 /path/to/update-firewall.sh --no-output --verbose
```

## Set up an Alias for the script

To make it easier to run the script you can set up an alias in your `.bashrc` or `.zshrc` file.

```bash
alias upfi="/path/to/update-firewall.sh"
# eg: alias upfi="/home/conor/scripts/api/update-firewall/update-firewall.sh"
```

Don't forget to source the file after adding the alias.

```bash
source ~/.bashrc
```

This will allow you to run the script by typing `upfi` in the terminal saving you time.

## Logging

The script logs all output to a log file in the `logs` directory. The log file is named `update-firewall.log` by default, but you can customize this in the `.env` file.

### Configuration

- **LOG_LOCATION**: The location of the log file, either relative or absolute path (it must end with a `/`). Default is `../../logs/`.
- **LOG_NAME**: The name of the log file. Default is `${script_name}.log`. You can change this to whatever you want, for example, `${script_name}_$(date +'%Y-%m-%d_%H-%M-%S').log`.
- **LOG_LEVEL**: The level of logging you want to use. Options are:
  - `STANDARD`: Basic logging including errors.
  - `VERBOSE`: Shows more information (output to terminal and log).
  - `EXTENDED`: Records the API response as well (output to terminal and log).
- **LOG_OVERWRITE**: A flag to determine if the log file should be overwritten each time the script is run. Default is `0` (append to the log file). Set to `1` to overwrite the log file each time.

### Default Configuration

```env
LOG_LOCATION="../../logs/"
LOG_NAME="${script_name}.log"
LOG_LEVEL="STANDARD"
LOG_OVERWRITE=1
```

## Future Improvements

- Add ability to add an IPv6 address automatically
- Add ability to programmatically add ids into the `env` file
- Better error handling and reporting
- Add Notifications via Webhooks
