#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2034
# Update Firewall Script - Main Script
# Location: api/update-firewall/update-firewall.sh
# Version 1.3.1
# Author: Conor Ryan
# Date: 2024-10-13
# Description: This script will update the IP address of a firewall on Digital Ocean and Cloudflare
####

script_name="update-firewall"

# Set the current directory
cd "$(dirname "$0")" || exit 1

# Load .env file
if [ ! -f ".update-firewall.env" ]; then
  echo -e "No '.update-firewall.env' file found\nMake sure that you have copied from the sample file into the script directory first before executing this script\n\ncp sample.update-firewall.env .update-firewall.env\n\nThe script will now exit";
  exit 1;

fi

set -o allexport
source .update-firewall.env
set +o allexport

# Set Internal ENVs
ip_changed=0

# Add functions
source ./functions/function-list.sh

# Script Prerequisites
source ./pre/script-checks.sh

# Script Mode
source ./pre/script-mode.sh

# IP Checks
source ./pre/ip-checks.sh

# Firewall Update
source ./main/firewall-update.sh

# Save IP
source ./post/set-ip.sh

# Email Notification Post Script
source ./post/email-notification.sh

log_standard "Script Completed Successfully"

# Script Completes without error
exit 0;