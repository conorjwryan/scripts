#!/bin/bash
# shellcheck disable=SC1091,SC2034,SC2154
# Update Firewall Script - Firewall Update
# Location: api/update-firewall/main/firewall-update.sh
# Version 2.0.0
# Author: Conor Ryan
# Date: 2024-10-15
# Description: This script will work through the loop to update the firewalls
####

  if [[ $test == 1 ]]; then
    log "Test Mode: Skipping firewall update"
    return 0;
  fi

# Iterate through firewall accounts
index=1
while true; do
  eval "firewall_name=\$FIREWALL_NAME_$index"
  eval "firewall_active=\$FIREWALL_ACTIVE_$index"
  eval "firewall_service=\$FIREWALL_SERVICE_$index"
  eval "firewall_api_token=\$FIREWALL_API_TOKEN_$index"
  eval "firewall_account_id=\$FIREWALL_ACCOUNT_ID_$index"
  eval "firewall_group_id=\$FIREWALL_GROUP_ID_$index"
  eval "firewall_group_name=\$FIREWALL_GROUP_NAME_$index"

  if [[ -z $firewall_name ]]; then
    break
  fi

 if [[ -z $firewall_active ]]; then
    log_verbose "Firewall $firewall_name is not active"
    index=$((index + 1))
    continue;
  fi

  if [[ -z $cidr ]]; then
    cidr="32"
  fi

  log_verbose "Updating $firewall_name ($firewall_service)"

  update_firewall "$firewall_service" "$ip" "$cidr" "$firewall_api_token" "$firewall_account_id" "$firewall_group_id" "$firewall_group_name"
  
  index=$((index + 1))
done