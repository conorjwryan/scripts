#! /bin/bash
# shellcheck disable=SC1091,SC2154
# Update Firewall Script - Token Check
# Location: api/update-firewall/functions/token-check.sh
# Version 1.0.5
# Author: Conor Ryan
# Date: 2024-02-11
# Description: This script checks to see if the API token is set
####

token_check () {

  if [[ "$1" == "do" ]]; then
    token_name="Digital Ocean"
    token_ev="DO_API_TOKEN=0"
    token_check=$DO_ACTIVE

  elif [[ "$1" == "cf" ]]; then
    token_name="Cloudflare"
    token_ev="cf_token=0"
    token_check=$CF_ACTIVE;

  fi

  if [[ -z $token_check ]]; then
  log "ERROR: No $token_name API token provided!";
  log "ERROR: Please provide one in the .firewall.env file";
  log "ERROR: If you do not want to use $token_name set '$token_ev' in your .update-firewall.env' file"

  fi

}