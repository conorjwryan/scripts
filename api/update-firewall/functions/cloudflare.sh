#!/bin/bash
# shellcheck disable=SC1091,SC2154
# Update Firewall Script - Cloudflare Function
# Location: api/update-firewall/functions/cloudflare.sh
# Version 2.0.0
# Author: Conor Ryan
# Date 2024-10-15
# Description: This function will update the Cloudflare Firewall
####

cf_rule() {
  local ip_update=$1;
  local ip_cidr=$2;
  local api_token=$3;
  local account_id=$4;
  local group_id=$5;
  local group_name=$6;
  
  ip_update="${ip_update}/${ip_cidr}";

  log_verbose "Updating Cloudflare Access Group Settings\n";
  
  # token_check cf;

  cf_curl_output=$(curl -sS -X PUT "https://api.cloudflare.com/client/v4/accounts/${account_id}/access/groups/${group_id}" -H "Authorization: Bearer ${api_token}" -H "Content-Type:application/json" -d '{"precedence":1,"decision":"bypass","name":"'"${group_name}"'","include":[{"ip": {"ip": "'"$ip_update"'"}}]}');

  cf_status_success=$(echo "$cf_curl_output" | jq -r ".success");

  if [[ ${cf_status_success} != "true" ]]; then
    log "ERROR: Something is wrong with the API request.";
    log_api "$cf_curl_output";

  fi

  log_api "$cf_curl_output"

}