#! /bin/bash
# shellcheck disable=SC1091,SC2154
# Update Firewall Script - Send Notification
# Location: api/update-firewall/functions/send-notification.sh
# Version 3.0.1_1
# Author: Conor Ryan
# Date: 2024-10-13
# Description: This script will send an email notification if the IP address has changed
####

####
# send_notification
####
# This function will send an email notification if the IP address has changed
# It will check to see if email is active and if it is, it will check to see if the email command is set to 'sendgrid', 'mail' or 'ntfy-curl'
####

send_notification () {

  old_ip=$1
  new_ip=$2

  if [[ ${EMAIL_ACTIVE} == 0 ]]; then
    log_verbose "Email Skipped!";
    log_verbose "INFO: If you would like to run this script set 'EMAIL_ACTIVE=1' in your env file";
    log_verbose "INFO: At this time only 'Sendgrid' and 'mail' are supported"
    return 0;

  elif [[ -z ${EMAIL_COMMAND} ]]; then
    log "ERROR: No email command provided!"
    log "ERROR: Please provide one in the .update-firewall.env file";
    log "ERROR: If you do not want to use email set 'EMAIL_ACTIVE=0' the file"
    exit 1;

  fi 

  if [[ $1 == "test" ]]; then
    log "Sending Test Notification Email"

    EMAIL_SUBJECT="Test Notification Email"
    EMAIL_BODY="This is a test notification email"

  elif [[ $1 == "error" ]]; then
    log "Sending Error Notification Email"

    EMAIL_SUBJECT="Error Notification Email"
    EMAIL_BODY="An error has occurred: $2"
    
  fi
    
  if [[ -z $EMAIL_SUBJECT ]]; then
    EMAIL_SUBJECT="IP Address on ${HOSTNAME} Change Detected"
  fi

  if [[ -z $EMAIL_BODY ]]; then
    EMAIL_BODY="The IP address has changed from ${old_ip} to ${new_ip}"
  fi

  ####
  # SendGrid
  ####

  if [[ ${EMAIL_COMMAND} == "sendgrid" ]]; then

    if [[ -z ${SENDGRID_API} ]]; then
      log "ERROR: No Sendgrid API key provided!";
      log "ERROR: Please provide one in the .update-firewall.env file";
      log "ERROR: If you do not want to use Sendgrid set 'EMAIL_ACTIVE=0' the file"
      exit 1; 
    fi

    if [[ -z ${EMAIL_FROM} ]]; then
      log "ERROR: No email from address provided!";
      log "ERROR: Please provide one in the .update-firewall.env file";
      log "ERROR: If you do not want to use email set 'EMAIL_ACTIVE=0' the file"
      exit 1; 
    fi

    log_verbose "Sending Email Notification via SendGrid"

    curl --request POST \
        --url https://api.sendgrid.com/v3/mail/send \
        --header "Authorization: Bearer $SENDGRID_API" \
        --header 'Content-Type: application/json' \
        --data '{"personalizations": [{"to": [{"email": "'"$EMAIL_TO"'"}]}],"from": {"email": "'"$EMAIL_FROM"'"},"subject": "'"$EMAIL_SUBJECT"'","content": [{"type": "text/plain", "value": "'"$EMAIL_BODY"'"}]}'

  ####
  # Mail Command
  ####

  elif [[ ${EMAIL_COMMAND} == "mail" ]]; then

    if [[ -z ${EMAIL_FROM} ]]; then
      log "ERROR: No email from address provided!";
      log "ERROR: Please provide one in the .update-firewall.env file";
      log "ERROR: If you do not want to use email set 'EMAIL_ACTIVE=0' the file"
      exit 1; 
    fi

    if [[ -z ${EMAIL_TO} ]]; then
      log "ERROR: No email to address provided!";
      log "ERROR: Please provide one in the .update-firewall.env file";
      log "ERROR: If you do not want to use email set 'EMAIL_ACTIVE=0' the file"
      exit 1; 
    fi

    log_verbose "Sending Email Notification via MAIL Command\n";
    echo "${EMAIL_BODY}" | "${EMAIL_COMMAND}" -s "${EMAIL_SUBJECT}" -aFrom:"${EMAIL_FROM}" "${EMAIL_TO}";

  ####
  # NTFY-cURL
  ####

  elif [[ ${EMAIL_COMMAND} == "ntfy-curl" ]]; then
    
    if [[ -z ${NTFY_CURL_DIR} ]]; then
      log "ERROR: No NTFY_CURL_DIR provided!";
      log "ERROR: Ensure NTFY-cURL is set up correctly";
      log "ERROR: Refer to the README.md for more information";
      log "ERROR: If you do not want to use email set 'EMAIL_ACTIVE=0' the file"
      exit 1; 
    fi

    if [[ ! -f ${NTFY_CURL_DIR}/ntfy-curl.sh ]]; then
      log "ERROR: No NTFY_SCRIPT found!";
      log "ERROR: Ensure NTFY-cURL is set up correctly";
      log "ERROR: Refer to the README.md for more information";
      log "ERROR: If you do not want to use email set 'EMAIL_ACTIVE=0' the file"
      exit 1; 
    fi

    log_verbose "Sending Email Notification via NTFY-cURL";
    log_verbose "INFO: Notifications sent via NTFY-cURL use the email set in the NTFY-cURL script";

    if [[ $verbose -gt 1 ]]; then
      "${NTFY_CURL_DIR}"/ntfy-curl.sh --title="${EMAIL_SUBJECT}" --message="${EMAIL_BODY}" -e -v;
      if [[ $? -ne 0 ]]; then
        log "ERROR: Failed to send email notification via NTFY-cURL";
        log "ERROR: run ${NTFY_CURL_DIR}/ntfy-curl.sh --title='${EMAIL_SUBJECT}' --message='${EMAIL_BODY}' --email --verbose for more information";
        exit 1;

      else 
        log_verbose "INFO: Notification sent via NTFY-cURL";
      fi

    else
      "${NTFY_CURL_DIR}"/ntfy-curl.sh --title="${EMAIL_SUBJECT}" --message="${EMAIL_BODY}" -e > /dev/null;
      if [[ $? -ne 0 ]]; then
        log "ERROR: Failed to send email notification via NTFY-cURL";
        log "ERROR: run ${NTFY_CURL_DIR}/ntfy-curl.sh --title='${EMAIL_SUBJECT}' --message='${EMAIL_BODY}' --email --verbose for more information";
        exit 1;
      else 
        log_verbose "INFO: Notification sent via NTFY-cURL";
      fi
    fi

  ####
  # Custom Command
  ####

  elif [[ ${EMAIL_COMMAND} == "custom" ]]; then

    if [[ -z ${CUSTOM_COMMAND} ]]; then
      log "ERROR: No custom command provided!";
      log "ERROR: Please provide one in the .update-firewall.env file";
      log "ERROR: If you do not want to use email set 'EMAIL_ACTIVE=0' the file"
      exit 1; 
    fi

    log_verbose "Sending Notification via ${CUSTOM_CMD_NAME}\n";

    ${CUSTOM_CMD}

    if [[ $? -ne 0 ]]; then
      log "ERROR: Failed to send email notification via ${CUSTOM_CMD_NAME}";
      log "ERROR: run ${CUSTOM_CMD} for more information";
      exit 1;

    else 
      log_verbose "INFO: Notification sent via ${CUSTOM_CMD_NAME}";
    fi

  fi

}