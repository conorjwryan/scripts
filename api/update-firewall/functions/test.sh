#!/bin/bash
# shellcheck disable=SC1091,SC2034,SC2154
# Update Firewall Script - Testing Mode
# Location: api/update-firewall/functions/test.sh
# Version 1.0.1
# Author: Conor Ryan
# Date: 2024-10-13
# Description: This script will send a test notification email

test() {

  log "Starting Test Mode"
  log "Subject: Test Notification Email"
  log "Body: This is a test notification email"
  
  send_notification "test"

  log "Email sent successfully"
}