#!/bin/bash
# shellcheck disable=SC1091,SC2154
# Update Firewall Script - Update Firewall Function
# Location: api/update-firewall/functions/update-firewall.sh
# Version 1.0.0
# Author: Conor Ryan
# Date 2024-10-15
# Description: This function process through the firewalls to update
####

update_firewall() {
  local service=$1
  local ip=$2
  local cidr=$3
  local api_token=$4
  local account_id=$5
  local group_id=$6
  local group_name=$7

  
  if [[ $service == "cf" || $service == "CF" ]]; then
    cf_rule "$ip" "$cidr" "$api_token" "$account_id" "$group_id" "$group_name"
  elif [[ $service == "do" || $service == "DO" ]]; then
    do_rule "$ip" "$cidr" "$api_token" "$group_id" "$group_name"
  else
    log "ERROR: Unsupported firewall service: $service"
    log "ERROR: Please check the .env configuration and try again"
    log "Skipping..."
  fi
}