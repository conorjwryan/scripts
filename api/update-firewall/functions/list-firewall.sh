#!/bin/bash
# shellcheck disable=SC1091,SC2154
# Update Firewall Script - List Firewall
# Location: api/update-firewall/functions/list-firewall.sh
# Version 2.0.0
# Author: Conor Ryan
# Date: 2024-10-15
# Description: This script will list out the firewalls on Digital Ocean and Cloudflare
####

list_firewall () {
  log "Welcome! This script will now list out your firewalls associated with the information provided in your .env file\n";

  index=1
  while true; do
    eval "firewall_name=\$FIREWALL_NAME_$index"
    eval "firewall_active=\$FIREWALL_ACTIVE_$index"
    eval "firewall_service=\$FIREWALL_SERVICE_$index"
    eval "firewall_api_token=\$FIREWALL_API_TOKEN_$index"
    eval "firewall_account_id=\$FIREWALL_ACCOUNT_ID_$index"
    eval "firewall_group_id=\$FIREWALL_GROUP_ID_$index"
    eval "firewall_group_name=\$FIREWALL_GROUP_NAME_$index"

    if [[ -z $firewall_name ]]; then
      break
    fi

    log "Listing Firewall for Firewall $index ($firewall_name - $firewall_service)"

    if [[ $firewall_active != 1 ]]; then
      log_verbose "Firewall $firewall_name is not active"
      index=$((index + 1))
      continue
    fi

    if [[ $firewall_service == "do" ]]; then
      log "Searching for Firewalls on Digital Ocean Account"
      echo ""

      do_firewall_check=$(curl -s -X GET "https://api.digitalocean.com/v2/firewalls/" -H "Authorization: Bearer ${firewall_api_token}" -H "Content-Type: application/json")

      do_api_error=$(jq 'has("id")' <<< "$do_firewall_check")

      do_firewall_list=$(echo "$do_firewall_check" | json_pp | tee -a "$log_file")

      if [[ ${do_api_error} == "true" ]]; then
        log "Something is wrong with the API request."
        log_api "${do_firewall_check}"
        echo ""
        log "No firewalls found! In order to use this script please add a firewall to your Digital Ocean account."
        exit 1
      fi

      for k in $(jq ".firewalls|keys|.[]" <<< "$do_firewall_list"); do
        value=$(jq -r ".firewalls[$k]" <<< "$do_firewall_list")
        do_firewall_list_id=$(jq -r '.id' <<< "$value")
        do_firewall_list_name=$(jq -r '.name' <<< "$value")
        printf '%s\t%s\t\n' "$do_firewall_list_name" "$do_firewall_list_id"
      done | column -t -s$'\t'
      echo -e "\n"

    elif [[ $firewall_service == "cf" ]]; then

      log "Searching for ACLs on Cloudflare Account\n"
      echo ""

      cf_firewall_check=$(curl -sS -X GET "https://api.cloudflare.com/client/v4/accounts/${firewall_account_id}/access/groups/" -H "Authorization: Bearer ${firewall_api_token}" -H "Content-Type:application/json")

      cf_status_success=$(echo "$cf_firewall_check" | jq -r ".success")

      if [[ ${cf_status_success} != "true" ]]; then
        log "Something is wrong with the API request"
        echo ""
        log_api "$cf_firewall_check"
        exit 1
      fi

      cf_firewall_list=$(echo "$cf_firewall_check" | json_pp | tee -a "$log_file")

      for k in $(jq ".result|keys|.[]" <<< "$cf_firewall_list"); do
        value_2=$(jq -r ".result[$k]" <<< "$cf_firewall_list")
        cf_firewall_list_id=$(jq -r '.id' <<< "$value_2")
        cf_firewall_list_name=$(jq -r '.name' <<< "$value_2")
        printf '%s\t%s\t\n' "$cf_firewall_list_name" "$cf_firewall_list_id"
      done | column -t -s$'\t'
      echo -e "\n"

    else
      log "ERROR: Unsupported firewall service: $firewall_service"
      log "ERROR: Please check the .env configuration and try again"
      log "Skipping..."
    fi

    index=$((index + 1))
  done

  ####
  # End of List Firewalls
  ####

}