#!/bin/bash
# shellcheck disable=SC1091,SC2034,SC2154
# Update Firewall Script - Help
# Location: api/update-firewall/functions/help.sh
# Version 1.0.1
# Author: Conor Ryan
# Date: 2024-10-13
# Description: This script will display the help menu for the update firewall script

help() {
  echo -e "Help Menu"
  echo -e "Usage: ./${script_name}.sh [--verbose|-v] [--no-output] [--test|-t] [--help] [--list-firewalls|--list|-l] [IP ADDRESS]"
  echo -e ""
  echo -e "Options:"
  echo -e "--verbose, -v\t\t\tEnable verbose mode"
  echo -e "--verbose-api, -va\t\tEnable verbose mode for API calls"
  echo -e "--test, -t\t\t\tSend Test notification email"
  echo -e "--list-firewalls, --list, -l\tList all firewalls"
  echo -e "--help, -h\t\t\tDisplay this help message"
  echo -e "--no-output\t\t\tDisable all output from default script (Except Errors)"
  echo -e "";
  echo -e "Examples:"
  echo -e "./${script_name}.sh (Auto-detect IP Address and run script)"
  echo -e "./${script_name}.sh --no-output (Run script with no output except for errors)"
  echo -e "./${script_name}.sh --verbose 1.2.3.4 (Enables verbose mode and sets IP Address to 1.2.3.4)"
  echo -e "./${script_name}.sh -l (Shows all firewalls)"
  echo -e 
  echo -e "Note: If no IP ADDRESS is provided, the script will attempt to auto-detect it."

}