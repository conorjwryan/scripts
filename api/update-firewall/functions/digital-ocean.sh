#!/bin/bash
# shellcheck disable=SC1091,SC2154
# Update Firewall Script - Digital Ocean Function
# Location: api/update-firewall/functions/digital-ocean.sh
# Version 2.0.0
# Author: Conor Ryan
# Date: 2024-10-15
# Description: This function will update the Digital Ocean Firewall with the new IP address
####

do_rule() {
  local ip_update=$1;
  local ip_cidr=$2;
  local api_token=$3;
  local group_id=$4;
  local group_name=$5;
  
  ip_update="${ip_update}/${ip_cidr}";

  log_verbose "Updating Digital Ocean Firewall\n";

  # token_check "do";

  do_curl_output=$(curl -s -X GET "https://api.digitalocean.com/v2/firewalls/$group_id" -H "Authorization: Bearer $api_token" -H "Content-Type: application/json");

  log_api "${do_curl_output}"

  do_status_droplet=$(echo "$do_curl_output" | jq -r ".firewall.status");

  if [[ ${do_status_droplet} != "succeeded" ]]; then
    log "ERROR: Something is wrong with the API request.";
    log_api "${do_curl_output}";

  fi

  log_api "\nSuccess! API Works! Updating Firewall to $ip_update\n";

  do_current_droplets=$(echo "$do_curl_output" | jq -r ".firewall.droplet_ids | @csv");

  do_update_firewall_curl=$(curl -sS -X PUT "https://api.digitalocean.com/v2/firewalls/${group_id}" -H "Authorization: Bearer ${api_token}" -H "Content-Type:application/json" --data '{"name":"'"$group_name"'","inbound_rules":[{"protocol": "tcp","ports": "22","sources":{"addresses": ["'"$ip_update"'"]}}],"droplet_ids": ["'"$do_current_droplets"'"]}');

  log_api "${do_update_firewall_curl}";

  do_api_error=$(jq 'has("id")' <<< "$do_update_firewall_curl");

  if [[ ${do_api_error} == "true" ]]; then
    log "ERROR: Something is wrong with the API request.\n";

    do_api_error_message=$(jq -r '.message' <<< "$do_update_firewall_curl");
    
    log_api "$do_update_firewall_curl";

    if [[ $do_api_error_message == *"invalid address"* ]]; then
      log "ERROR: IP address is invalid. Please make sure it contains numbers only in the style of xxx.xxx.xxx.xxx like: 192.192.168.0.1";

    elif [[ $do_api_error_message == *"missing name"* ]]; then 

    log "\nThis error normally occurs not because the 'name' is incorrect but because the firewall has no droplet's associated with it. Correct this on the web interface to continue";

    fi

  fi
}