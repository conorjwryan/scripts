#!/bin/bash
# shellcheck disable=SC1091
# Update Firewall Script - Function List
# Location: api/update-firewall/functions/function-list.sh
# Version 1.3.0
# Author: Conor Ryan
# Date: 2024-10-15
# Description: This script includes the functions for the update-firewall.sh script
####

# Log Function
source ./functions/logs.sh

# Email Notification Function
source ./functions/send-notification.sh

# API Token Check
source ./functions/token-check.sh

# List Firewalls
source ./functions/list-firewall.sh

# Update Firewalls
source ./functions/update-firewall.sh

# Help Menu
source ./functions/help.sh

# Test Mode
source ./functions/test.sh

# Digital Ocean Firewall Update
source ./functions/digital-ocean.sh

# Cloudflare Firewall Update
source ./functions/cloudflare.sh
