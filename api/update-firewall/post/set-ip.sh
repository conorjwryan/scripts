#!/bin/bash
# shellcheck disable=SC1091,SC2154
# Update Firewall Script - Email Notification Logic
# Location: api/update-firewall/post/set-ip.sh
# Version 1.0.0
# Author: Conor Ryan
# Date: 2024-02-12
# Description: This script wil save the IP address to the .env file

if [[ -z "$1" ]]; then 
  $SCOMMAND -i 's/^CURRENT_IP=.*/CURRENT_IP="'"$ip"'";/' .update-firewall.env

else
  $SCOMMAND -i 's/^CURRENT_IP=.*/CURRENT_IP="'"$ip"'";/' .update-firewall.env

fi