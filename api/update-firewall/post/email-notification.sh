#!/bin/bash
# shellcheck disable=SC1091,SC2154
# Update Firewall Script - Email Notification Logic
# Location: api/update-firewall/post/email-notification-logic.sh
# Version 1.1.0
# Author: Conor Ryan
# Date: 2024-11-11
# Description: This script will send an email notification if the IP address has changed
####

if [[ ${ip_changed} == 1 ]]; then

    if [[ -z "${WAIT_TIME}" ]]; then
        WAIT_TIME=30;
    fi
    
    log "Waiting ${WAIT_TIME} seconds before sending email notification";

    sleep "${WAIT_TIME}";

    log "The IP has been changed since this script was last run";
    log "From $CURRENT_IP to $ip";
    
    if [[ ${EMAIL_ACTIVE} == 1 ]]; then
        log_verbose "Sending Email Notification to ${EMAIL_TO}";
        send_notification "$CURRENT_IP" "$ip";

    fi

fi