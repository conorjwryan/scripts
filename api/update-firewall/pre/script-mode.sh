#!/bin/bash
# shellcheck disable=SC1091,SC2034,SC2154
# Update Firewall Script - Script Mode
# Location: api/update-firewall/pre/script-mode.sh
# Version 2.1.1
# Author: Conor Ryan
# Date: 2024-10-13
# Description: This script works out the logic for the arguments passed to the script
####

verbose="0"
help="0"
test="0"
list="0"
no_output="0"
ip=""


# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
  case $1 in
    --verbose|-v) verbose="1"; shift ;;
    --verbose-api|-va) verbose="2"; shift ;;
    --help|-h) help="1"; shift ;;
    --list-firewalls|--list|-l) list="1"; shift ;;
    --test|--email-test|-t) test="1"; shift ;;
    --no-output) no_output="1"; shift ;;
    *) ip="$1"; shift ;;
  esac
done

if [[ ${help} == "1" ]]; then
  help;
  exit 0
fi

if [[ ${test} == "1" ]]; then
  test;
  exit 0
fi

if [[ ${list} == "1" ]]; then
  list_firewall;
  exit 0

fi

if [[ ${verbose} == 2 ]]; then
  log_api "Enhanced Verbose Mode Enabled"
elif [[ ${verbose} == 1 ]]; then
  log_verbose "Verbose Mode Enabled"
fi

# Start Default Script Mode

if [[ "$ip" =~ [a-zA-Z] ]]; then
  log "ERROR: The IP Address is not valid. Please check the IP Address and try again."
  log "ERROR: The script will now exit"
  exit 1
fi

# Determine if IP Address is provided or not

if [[ -z "$ip" ]]; then

  log_verbose "INFO: No IP Address provided. Attempting to auto-detect IP Address"

  ip=$(dig +short txt ch whoami.cloudflare @1.0.0.1 | tr -d '"')

  if [[ -z ${ip} ]]; then
    log "ERROR: Failed to resolve the IP Address of ${HOSTNAME}."
    exit 1
  fi

  log_standard "IP Address Automatically Chosen to Update Forewall(s) is ${ip}/32"

else
  if [[ $ip == *"/"* ]]; then
    ip=$(echo "$ip" | cut -d'/' -f1)
    cidr=$(echo "$ip" | cut -d'/' -f2)
    ip_type="cidr"

  else
    cidr="32"
  
  fi

  log_standard "IP Address Manually Chosen to Update Firewall(s) is ${ip}/${cidr}"

fi

log_verbose ""
log_standard "Updating Firewall(s)"
log_verbose ""
