#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2034
# Update Firewall Script - Script Checks
# Location: api/update-firewall/pre/script-checks.sh
# Version 1.1.0
# Author: Conor Ryan
# Date: 2024-02-12
# Description: This script will check the script prerequisites are met before progressing
####

# Check for jq
if ! command -v jq &> /dev/null; then
    log "ERROR: 'jq' could not be found. Please install jq and try again.";
    log "ERROR: The script will now exit";
    exit 1;
fi

# Check for cURL
if ! command -v curl &> /dev/null; then
    log "ERROR: 'cURL' could not be found. Please install cURL and try again.";
    log "ERROR: The script will now exit";
    exit 1;

fi

# Check for DIG
if ! command -v dig &> /dev/null; then
    log "ERROR: 'dig' could not be found. Please install dig and try again.";
    log "ERROR: The script will now exit";
    exit 1;

fi

