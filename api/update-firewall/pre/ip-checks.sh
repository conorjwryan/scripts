#!/bin/bash
# shellcheck disable=SC1091,SC2034,SC2154
# Update Firewall Script - Script Mode
# Location: api/update-firewall/pre/ip-checks.sh
# Version 1.1.0
# Author: Conor Ryan
# Date: 2024-02-12
# Description: This script will check the IP Address and CIDR are valid before progressing
####

# Check if the IP Address is valid
if ! [[ $ip =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    log "ERROR: The IP Address is not valid. Please check the IP Address and try again.";
    log "ERROR: The script will now exit";
    exit 1;

fi

# Check if the CIDR is valid
if [[ ${ip_type} == "cidr" ]]; then
    # Check if the CIDR is valid
    if ! [[ $cidr =~ ^[0-9]+$ ]]; then
        log "ERROR: The CIDR is not valid. Please check the CIDR and try again.";
        log "ERROR: The script will now exit";
        exit 1;

    fi

fi

# See if IP has changed from ENV value
if [[ ${CURRENT_IP} != "$ip" ]]; then
    ip_changed=1;

fi

# Check if DO and CF are active
if [[ ${DO_ACTIVE} == 0 && ${CF_ACTIVE} == 0 ]]; then 
    log "ERROR: Both functions are inactive and the script did not run.";
    log "ERROR: See '.env' to change the script settings"
    exit 1;

fi