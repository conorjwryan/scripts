#!/bin/bash
# shellcheck disable=SC1091,SC2034,SC2154
# Remove Kernels Script - Main Script
# Location: system/remove-kernels/remove-kernels.sh
# Version 2.1.0_3
# Author: Conor Ryan
# Date: 2024-10-19
# Description: Script to remove old kernels from the system
####

script_name="remove-kernels"

# Get the currently in-use kernel
uname -a
IN_USE=$(uname -r | cut -d'-' -f1-2)
echo -e "Your in-use kernel is $IN_USE\n"

# List all installed kernel-related packages except the in-use kernel and linux-generic
OLD_KERNELS=$(
dpkg --list |
grep -v "$IN_USE" |
grep -Ei 'linux-image|linux-headers|linux-modules|linux-tools' |
grep -v -E 'linux-tools-common|linux-(generic|headers-generic|image-generic|tools-generic)' |
awk '{ print $2 }'
)

# Check if there are any old kernels to remove
if [ -z "$OLD_KERNELS" ]; then
    echo -e "No old kernels to remove."
    echo -e "Exiting..."
    exit 0
fi

# Check if exec argument is passed
if [ "$1" == "exec" ]; then

    # Check if script is run as root
    if [ "$(id -u)" -ne 0 ]; then
        echo "This script must be run as root. Please use sudo."
        exit 1
    fi

    # Check if there are any old kernels to remove
    if [ -z "$OLD_KERNELS" ]; then
        echo -e "No old kernels to remove."
        echo -e "Exiting..."
        exit 0
    fi

    echo -e "WARNING: This action is dangerous and should not be done unless you know what you're doing."
    echo -e "You are about to remove the following kernels: "
    echo -e "$OLD_KERNELS\n"
    read -p "Do you want to continue? (yes/no): " response

    if [[ "$response" != "yes" && "$response" != "y" ]]; then
        echo "Operation aborted."
        exit 1
    fi

    echo -e "Removing old kernels..."
    echo -e "There is no need to interact with the script from this point onwards regardless of what is said on the screen. The script will complete the operation and exit.\n"

    sleep 2;

    for PACKAGE in $OLD_KERNELS; do
        yes | apt purge "$PACKAGE"
    done
    
    echo -e "\nKernel removal complete";
    echo -e "Exiting...";
else
    # Check if there are any old kernels to remove
    echo -e "Old Kernels to be removed: "
    echo -e "$OLD_KERNELS"
    echo -e "\nOnce you have double checked the list and are happy to proceed, run the script with the 'exec' argument."
    echo -e "Example: sudo $script_name exec"
fi

exit 0