# Remove Old Kernels Script

This script is used to remove old Linux kernels from your system. It is useful for freeing up disk space by removing unused kernel versions. This script is intended for Ubuntu/Debian-based distributions as it uses `apt` for package management.

## WARNING

Do not run this script if you are not sure what you are doing. Removing the wrong kernel can render your system un-bootable. Always make sure you have a backup of your system before running this script.

## Acknowledgements

The original inspiration for this script came from this StackOverflow post. [How to easily remove old kernels in Ubuntu 20.04 LTS](https://askubuntu.com/questions/1253347/how-to-easily-remove-old-kernels-in-ubuntu-20-04-lts). The script has been modified and improved since then.

## How to Setup the Script

The following instructions detail the setup required to use this script.

### Required Software

There are no additional software requirements for this script. It uses `dpkg` and `apt`, which are available by default on Ubuntu/Debian-based distributions.

Administrator (`sudo`) access is required to remove the old kernels. Only listing the old kernels can be done without `sudo`.

### Script Setup and Configuration

1. Clone this repository to your local machine and run the script from the command line.

    ```bash
    git clone https://gitlab.com/conorjwryan/scripts.git
    ```

    Enter the `remove-kernels` directory.

    ```bash
    cd scripts/system/remove-kernels
    ```

2. The script should be executable by default, but if it is not, make it executable:

    ```bash
    chmod +x remove-kernels.sh
    ```

## How to Use the Script

To use this script, simply run it from the command line. By default, it performs a dry run, listing the old kernels that would be removed. To actually remove the old kernels, you need to run the script with `sudo` and the `exec` parameter.

### Before Running the Script (IMPORTANT)

This script should be run after a system update that installs a new kernel. This ensures that the script does not remove the currently running kernel.

See the [Important Notes](#important-notes) section for more information.

### Dry Run (Listing Old Kernels)

Run the script without any parameters to perform a dry run:

```bash
./remove-kernels.sh
```

This will list the old kernels (and associated packages tools, headers, modules) that would be removed without actually removing them. This is important to verify that the script is targeting the correct kernels for removal.

### Execute Removal

To actually remove the old kernels, run the script with `sudo` and the `exec` parameter:

```bash
sudo ./remove-kernels.sh exec
```

A user prompt for confirmation will be displayed before removing the old kernels. Type `y` and press `Enter` to proceed with the removal. Past this point, no further user interaction is required. It will automatically remove the old kernels and associated packages.

## Important Notes

### Only Run After installing a new Kernel Reboot The System Before Running The Script

This script will not remove the currently running kernel or the latest kernel version. It will only remove unused old kernels. So if you have downloaded the latest kernel but not rebooted, it will remove the new kernel instead.

Follow the steps below to ensure the script works correctly:

1. Install the latest kernel update using `apt` or `apt-get`.

    ```bash
    sudo apt update && sudo apt upgrade
    sudo apt install linux-generic
    ```

2. Reboot the system to ensure the new kernel is running.

    ```bash
    sudo reboot
    ```

3. Run the script.

    ```bash
    ./remove-kernels.sh
    ```

### Use at Your Own Risk

This script is provided as-is and without warranty or liability for anything that might happen. Use it at your own risk!

It is always recommended to back up your system before making any changes especially when removing old kernels.

### During Execution No Further User Interaction is Required

After giving the initial confirmation of the `exec` process there is no need to press `y` and `Enter` if asked. The script will automatically remove the old kernels and associated packages without further user interaction.

The script will end with the following message:

```bash
"Kernel removal complete".
```
