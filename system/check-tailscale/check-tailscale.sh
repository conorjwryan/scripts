#!/bin/bash
# shellcheck disable=SC1091,SC2034,SC2154
# Check Tailscale Status - Main Script
# Location: system/check-tailscale/check-tailscale.sh
# Version 1.0.1
# Author: Conor Ryan
# Date: 2024-10-06
# Description: Script to check Tailscale connection status and send notification if issue detected
####

ntfy=0
verbose=0
help=0
email=0

# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        --ntfy|-n) ntfy=1; shift ;;
        --verbose|-v) verbose=1; shift ;;
        --email|-e) email=1; shift ;;
        --help|-h) help=1; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
done

if [[ $help == 1 ]]; then
    echo -e "Usage: ./check-tailscale.sh [--ntfy|-n] [--verbose|-v] [--email|-e] [--help|-h]"
    echo -e "Options:"
    echo -e "  --ntfy, -n      Send notification if an issue is detected"
    echo -e "  --verbose, -v   Enable verbose output"
    echo -e "  --email, -e     Send email notification as well"
    echo -e "  --help, -h      Show this help message"
    exit 0
fi

# Function to send notification
send_notification() {
    echo "Sending to NTFY Notification Service..."

    if [ -z ${NTFY_LOC} ]; then
        NTFY_LOC=$HOME/scripts/api/ntfy-curl/
    fi

    if [ ${verbose} -eq 1 ]; then
        if [ ${email} -eq 1 ]; then
            ${NTFY_LOC}ntfy-curl.sh --title="Tailscale Alert" --message="Tailscale issue on $(hostname) as of $(date): $1" --verbose --email
        else
            ${NTFY_LOC}ntfy-curl.sh --title="Tailscale Alert" --message="Tailscale issue on $(hostname) as of $(date): $1" --verbose
        fi
    else
        if [ ${email} -eq 1 ]; then
            ${NTFY_LOC}ntfy-curl.sh --title="Tailscale Alert" --message="Tailscale issue on $(hostname) as of $(date): $1" --email > /dev/null
        else
            ${NTFY_LOC}ntfy-curl.sh --title="Tailscale Alert" --message="Tailscale issue on $(hostname) as of $(date): $1" > /dev/null
        fi
    fi
}

# Check if Tailscale is running
if ! pgrep -x "tailscaled" > /dev/null; then
    echo "Tailscale process is not running"

    if [ ${ntfy} -eq 1 ]; then
        send_notification "Tailscale process is not running"
    fi
else
    echo "Tailscale process is running. Checking connection status..."

    # Check Tailscale connection status
    if tailscale status | grep -q "Logged out."; then
        echo "Tailscale is running but not logged in"

        if [ ${ntfy} -eq 1 ]; then
            send_notification "Tailscale is running but not logged in"
        fi
    else
        # Check if Tailscale is connected to network
        if ! ping -c 1 100.100.100.100 &> /dev/null; then
            echo "Tailscale is connected but unable to reach Tailscale network"

            if [ ${ntfy} -eq 1 ]; then
                send_notification "Tailscale is connected but unable to reach Tailscale network"
            fi
        else
            echo "Tailscale is running and connected properly."
        fi
    fi
fi