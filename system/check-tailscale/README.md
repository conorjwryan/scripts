# Check Tailscale Status Script

This script is used to check the status of the Tailscale service and send notifications if any issues are detected. It is useful for monitoring the Tailscale connection and ensuring that it is running and connected properly.

## How to Setup the Script

The following instructions detail the required software, information, and setup required to use this script.

It can be run on both Linux and macOS. For Mac, you will need to install the required software using [Homebrew](https://brew.sh/).

### Required Software

- `Tailscale` - this script uses Tailscale to check the connection status. You can install it using your package manager or follow the instructions [here](https://tailscale.com/download).

If the above software is not installed, the script will not run.

### Optional Software

- `cURL` (optional) - this script uses `cURL` to send notifications to NTFY. This is installed by default on most `Linux` distributions and `macOS`. If you do not have it installed, you can install it using your `package manager`.
- `configured NTFY-cURL script` (optional) - this script requires the NTFY-cURL script to be set up correctly. You can find the setup instructions in the [README.md](../../api/ntfy-curl/README.md) file.

### Required Information

For the script to run it does not require any additional information.

It is recommended that you have the NTFY-cURL script set up correctly to send notifications if any issues are detected. See the [README.md](../../api/ntfy-curl/README.md) file for more information.

### Script Setup and Configuration

1. Clone this repository to your local machine and run the script from the command line.

    ```bash
    git clone https://gitlab.com/conorjwryan/scripts.git
    ```

    Enter the `check-tailscale` directory.

    ```bash
    cd scripts/system/check-tailscale
    ```

2. The script should be executable by default, but if it is not, make it executable:

    ```bash
    chmod +x check-tailscale.sh
    ```

3. (Optional) Ensure that the `ntfy-curl.sh` script is set up correctly. You can find the setup instructions in the [README.md](../../api/ntfy-curl/README.md file.

## How to Use the Script

To use this script, simply run it from the command line with the required parameters.

```bash
./check-tailscale.sh [--ntfy|-n] [--verbose|-v] [--email|-e] [--help|-h]
```

### Parameters

- --ntfy, -n (optional) - send notification if an issue is detected.
- --verbose, -v (optional) - enable verbose output.
- --email, -e (optional) - send email notification as well.
- --help, -h (optional) - show usage information.

### Examples

Check Tailscale status (to terminal only):

```bash
./check-tailscale.sh
```

Check Tailscale status and send a notification if an issue is detected (requires NTFY-cURL setup):

````bash
./check-tailscale.sh --ntfy
````

Check Tailscale status with verbose output:

````bash
./check-tailscale.sh --verbose
````

Check Tailscale status and send an email notification if an issue is detected (requires NTFY-cURL setup):

````bash
./check-tailscale.sh --ntfy --email
````

## Future Improvements

- Better Error Handling - currently, even if the cURL request fails, the script will not provide detailed error messages. This will be fixed in the future.
