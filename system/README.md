# System Directory

The scripts in this directory are designed to help manage and monitor system services. They work through various command-line tools and scripts to perform their tasks.

In the interest of both privacy and system agnostic operation, any sensitive information (home directories / code folders and credentials) have been obscured and are accessed by the script calling the `.env` file in each of these directories.

## Scripts

### Tailscale Status Check

This script is for those who use [Tailscale](https://tailscale.com/). This script will check the status of the Tailscale service and send a notification if the service is not running. This is useful if you want to be notified if the Tailscale service stops running.

Uses the **`NTFY-cURL` script** ([located in](../api/ntfy-curl/README.md)) to send notifications/emails.

### Remove Old Kernels

This script is used to remove old Linux kernels from your system. It is useful for freeing up disk space on cloud servers. This script is intended for Ubuntu-based distributions only as it uses `apt` for package management.
