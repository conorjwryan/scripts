# Bash Scripts

This repository contains a collection of bash scripts I have written to help automate some of my daily tasks across my personal and client servers. I have written these scripts to be as system and platform agnostic as possible.

## Scripts

### Web Check

**Usage:** For those who use [Better Uptime](https://betteruptime.com/) to monitor their websites, this script will pause all monitors for a specified website. This is useful if you are doing maintenance on a website and do not want to be notified of any downtime.

### Update Firewall

**Usage:** For those who use [Digital Ocean](https://www.digitalocean.com/) and [Cloudflare](https://www.cloudflare.com/) to manage their DNS and firewall, this script will update the firewall to allow the IP address of the machine it is running on. This is useful if you are running a dynamic IP address and want to be able to access your server without having to update the firewall manually.

### Code Backup

**Usage:** This script is for those who want to backup their code and environment settings. These include a specified code directory like `home/user/code`, crontab and fstab files, ssh folder, bash/zsh profile and aliases, installed apt/brew programs, VSCode settings, and any other files/folders you specify. It is then compressed, encrypted and uploaded to a specified location (through RSync, RClone or locally).

### Tailscale ACL Download

**Usage:** This script is for those who use [Tailscale](https://tailscale.com/). This script will download the ACLs from Tailscale and save them to a specified location. This is useful if you want to keep a backup of your ACLs before a change.

### Tailscale Status Check

**Usage:** This script is for those who use [Tailscale](https://tailscale.com/). This script will check the status of the Tailscale service and send a notification if the service is not running. This is useful if you want to be notified if the Tailscale service stops running.

Uses the **`NTFY-cURL` script** to send notifications/emails.

### NTFY cURL

**Usage:** This script is for those who use [NTFY](https://ntfy.sh/) to send notifications. This script will send a notification to your device/email using cURL. This is useful if you want to send a notification from a script but can't install NTFY or configure Postfix machine.

### Remove Old Kernels

**Usage:** This script is used to remove old Linux kernels from your system. It is useful for freeing up disk space on cloud servers. This script is intended for Ubuntu/Debian-based distributions only as it uses `apt` for package management.

## How To Use

To use these scripts, simply clone this repository to your local machine and run the scripts from the command line.

```bash
git clone https://gitlab.com/conorjwryan/scripts.git
cd scripts
```

Each script is self-contained in their own directory and requires a `.env` file to be created in order for the script to run. All instructions for use can be found in the script directory. Enter the directory and then copy the `sample.env` file to `.env` and fill in the required information.

```bash
# Using the Web Check script as an example
cd api/web-check
cp sample.web-check.env .web-check.env
# Fill in the required information
nano .web-check.env
# Make the script executable
chmod +x web-check.sh
# Run the script
./web-check.sh
```

### Sending Notifications

The scripts have been written to send notifications using [NTFY](https://ntfy.sh/) via the `NTFY-cURL` script in this repository. This is a simple and easy to use notification system that can send notifications to your device or email via `cURL` without having `NTFY` installed on the machine.

Some scripts also allow you to send notifications through the mail command. This is useful if you have a mail server set up on your machine.

## Other Projects

I also maintain an [Ansible](https://gitlab.com/conorjwryan/ansible) script repository for automating processes across many servers.

For other projects I have worked on, check out my [Website](https://conorjwryan.com) and [GitLab](https://gitlab.com/conorjwryan).

Any feedback or suggestions are welcome.
