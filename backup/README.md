# Backup Directory

This directory contains scripts for backing up files and directories.

## Scripts

### Code Backup

**Usage:** This script is for backing up code files and directories. It uses `tar` to create a compressed archive of the files and `gpg` to encrypt the archive. The script is useful for backing up code files and directories that you want to keep private.

Explore the [code-backup](code-backup) directory for more information about setup.
