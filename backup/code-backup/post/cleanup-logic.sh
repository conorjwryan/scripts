#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Cleanup Logic - Code Backup Script
# Location: backup/code-backup/post/cleanup-logic.sh
# Version 1.0.1
# Author: Conor Ryan
# Date: 2024-10-25
# Description: Cleanup Logic for Code Backup Script
####

if [[ $1 != "first-run" ]]; then
    log_standard "Removing Encrypted TGZ\n";

    ${dbg} rm -rf "${backup_folder}.tgz.asc";

else 
    echo -e "First Run Completed\n\nConfirm that the backup folder contains all the necessary files and folders needed and then run delete the first-run folder:\n\nrm -rf ${local_copy_location}/*\n\nThen run the script again without any parameters\n\n./code-backup.sh\n\nThis script will now exit...\n";

fi