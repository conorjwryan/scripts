#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Logs Function - Code Backup Script
# Location: backup/code-backup/functions/logs.sh
# Version 1.0.1
# Author: Conor Ryan
# Date: 2024-10-27
# Description: Logs Functions for Code Backup Script
####


if [ ! -d "${log_location}" ]; then
    mkdir -p "${log_location}" 2>/dev/null

    if [ ! -d "${log_location}" ]; then
        echo -e "Error: Unable to create logs directory"
        echo -e "Error: Check permissions and try again"
        exit 1
    fi
fi

log_file="${log_location}${log_name}"

if [ ! -f "${log_file}" ]; then
        touch "${log_file}"

        if [ ! -f "${log_file}" ]; then
            echo -e "Error: Unable to create log file"
            echo -e "Error: Check permissions and try again"
            exit 1
        fi
fi

if [[ ${log_overwrite} == 1 ]]; then
    echo -n "" > "${log_file}"
fi

# Remove all newlines and escape sequences for log file
clean_log_message() {
    echo "$1" | tr -d '\n' | sed 's/\\n//g'
}

log() {
    echo -e "$1"
    clean_message=$(clean_log_message "$1")
    echo "$(date '+%Y-%m-%d %H:%M:%S') - ${clean_message}" >> "${log_file}"
}

log_api() {
    if [[ ${verbose} -ge 2 ]]; then
        echo -e "$1"
    fi

    if [[ ${log_level} == "EXTENDED" ]]; then
        clean_message=$(clean_log_message "$1")
        echo "$(date '+%Y-%m-%d %H:%M:%S') - ${clean_message}" >> "${log_file}"
    fi
}

log_verbose() {
    if [[ ${verbose} -ge 1 ]]; then
        echo -e "$1"
    fi

    if [[ ${log_level} == "VERBOSE" || ${log_level} == "EXTENDED" ]]; then
        clean_message=$(clean_log_message "$1")
        echo "$(date '+%Y-%m-%d %H:%M:%S') - ${clean_message}" >> "${log_file}"
    fi
}

log_standard() {
    if [[ ${verbose} -ge 1 ]]; then
        echo -e "$1"
    elif [[ ${no_output} == 0 ]]; then
        echo -e "$1"
    fi

    clean_message=$(clean_log_message "$1")
    echo "$(date '+%Y-%m-%d %H:%M:%S') - ${clean_message}" >> "${log_file}"
}