#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Process Transfer Functions - Code Backup Script
# Location: backup/code-backup/functions/process-remotes.sh
# Version 1.0.0
# Author: Conor Ryan
# Date: 2024-10-27
# Description: Process Remote Functions for Code Backup Script

####
# Process Local Transfer
####
process_local_transfer() {
    local dest_path=$1
    
    log_standard "Copying to Local Storage: ${dest_path}\n"
    ${dbg} mkdir -p "${dest_path}/"

    cd "${backup_folder_dir}" || error_process "Failed to change to backup folder directory" "exit"

    if [[ $first_run == 1 ]]; then
        cp -a "${backup_folder_dir}/" "${dest_path}/"
    else
        if [[ ! -d "$dest_path" ]]; then
            log_standard "Local Copy Location Does Not Exist\n"
            ${dbg} mkdir -p "${dest_path}/"
        fi
        find . -name '*.tgz.asc' -exec cp -a {} "$dest_path"/ \;
    fi

    log_verbose "Completed Local Copy Transfer to ${dest_path}\n"
}

####
# Process RClone Transfer
####
process_rclone_transfer() {
    local host=$1
    local path=$2

    log_standard "Copying via RClone to ${host}:${path}\n"
    ${dbg} rclone copy "${backup_folder_dir}" "${host}:${path}" -P
    log_standard "Completed RClone Transfer to ${host}:${path}\n"
}

####
# Process RSync Transfer
####
process_rsync_transfer() {
    local connection=$1
    local path=$2

    log_standard "Copying via RSync to ${connection}:${path}\n"
    ${dbg} rsync -rvXDP --ignore-existing --remove-source-files "${backup_folder_dir}/" "${connection}:${path}"
    log_standard "Completed RSync Transfer to ${connection}:${path}\n"
}

# Process a single remote transfer
process_remote() {
    local remote_num=$1
    local active_var="remote_${remote_num}_active"
    local name_var="remote_${remote_num}_name"
    local type_var="remote_${remote_num}_type"
    local host_var="remote_${remote_num}_host"
    local path_var="remote_${remote_num}_path"

    if [[ ${!active_var} != 1 ]]; then
        return 0
    fi

    log_standard "Processing transfer for ${!name_var}\n"

    case ${!type_var} in
        "local")
            process_local_transfer "${!path_var}" || return 1
            ;;
        "rclone")
            process_rclone_transfer "${!host_var}" "${!path_var}" || return 1
            ;;
        "rsync")
            process_rsync_transfer "${!host_var}" "${!path_var}" || return 1
            ;;
    esac

    return 0
}
