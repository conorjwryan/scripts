#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Validate Remote Configuration Function - Code Backup Script
# Location: backup/code-backup/functions/validate-remotes.sh
# Version 1.0.1
# Author: Conor Ryan
# Date: 2024-10-27
# Description: Validate Remote Configuration
####

validate_remote() {
    local remote_num=$1
    local active_var="remote_${remote_num}_active"
    local name_var="remote_${remote_num}_name"
    local type_var="remote_${remote_num}_type"
    local host_var="remote_${remote_num}_host"
    local path_var="remote_${remote_num}_path"

    if [[ ${!active_var} != 1 ]]; then
        log_verbose "Remote ${remote_num} (${remote_name}) is not active and will not be processed\n"
        return 0
    fi

    log_verbose "Validating remote ${remote_num} (${!name_var})\n"

    # Validate required fields
    if [[ -z "${!name_var}" ]]; then
        log_verbose "Remote ${remote_num} is active but name is not set\nRemote will not be processed\n"
        error_process "Remote ${remote_num} is active but name is not set" "continue"
        return 1
    fi

    if [[ -z "${!type_var}" ]]; then
        
        log_verbose "Remote ${remote_num} (${!name_var}) is active but type is not set\nRemote will not be processed\n"
        error_process "Remote ${remote_num} (${!name_var}) is active but type is not set" "continue"
        return 1
    fi

    if [[ -z "${!path_var}" ]]; then
        log_verbose "Remote ${remote_num} (${!name_var}) is active but path is not set\nRemote will not be processed\n"
        error_process "Remote ${remote_num} (${!name_var}) is active but path is not set" "continue"
        return 1
    fi

    case ${!type_var} in
        "local")
            # No additional validation needed for local
            ;;
        "rclone"|"rsync")
            if [[ -z "${!host_var}" ]]; then
                log_verbose "Remote ${remote_num} (${!name_var}) is active but host is not set\nRemote will not be processed\n"
                error_process "Remote ${remote_num} (${!name_var}) is active but host is not set" "continue"
                return 1
            fi
            ;;
        *)
            log_verbose "Remote ${remote_num} (${!name_var}) has invalid type: ${!type_var}\nRemote will not be processed\n"
            error_process "Remote ${remote_num} (${!name_var}) has invalid type: ${!type_var}" "continue"
            return 1
            ;;
    esac

    return 0
}