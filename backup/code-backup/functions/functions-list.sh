#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Functions List: Code Backup Script
# Location: backup/code-backup/functions/functions-list.sh
# Version 1.2.0
# Author: Conor Ryan
# Date: 2024-10-27
# Description: List of Functions for Code Backup Script
####

# Log Function
source ${script_location}/functions/logs.sh

# Validate Remotes Function
source ${script_location}/functions/validate-remotes.sh

# Process Remotes Function
source ${script_location}/functions/process-remotes.sh

# Send Notification Function
source ${script_location}/functions/send-notification.sh

# Process Errors Function
source ${script_location}/functions/process-errors.sh