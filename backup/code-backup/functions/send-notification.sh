#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Send Notification Function - Code Backup
# Location: backup/code-backup/functions/send-notification.sh
# Version 1.0.0
# Author: Conor Ryan
# Date: 2024-10-27
# Description: Script to send a notification using NTFY Notification Service
####

# Send Notification Function
send_notification () {

    local notification_type=$1

    if [[ ${notification_type} == "success" ]]; then
        title="Backup of ${hostname} Successful"
        message="Backup completed successfully on $(hostname) as of $(date)"

    elif [[ ${notification_type} == "error" ]]; then
            title="Backup Failure of $(hostname) due to Error"
            message="Backup failed on $(hostname) as of $(date) with error: $2"

    elif [[ ${notification_type} == "part-error" ]]; then
        title="Backup Completed on $(hostname) with Errors"
        message="Backup of $(hostname) Partially Completed as of $(date) with errors: $2"

    elif [[ ${notification_type} == "test" ]]; then
        title="Test Notification"
        message="This is a test notification from $(hostname) as of $(date)"
        verbose="1";

    else
        log_standard "Undefined Notification Type. Please Check Code.";
        return 1
    fi

    echo "Sending to NTFY Notification Service..."

    if [ -z ${NTFY_LOC} ]; then
        NTFY_LOC=$HOME/scripts/api/ntfy-curl/
    fi

    if [ ${verbose} -eq 1 ]; then
        if [ ${email} -eq 1 ]; then
            ${NTFY_LOC}ntfy-curl.sh --title="${title}" --message="${message}" --verbose --email
        else
            ${NTFY_LOC}ntfy-curl.sh --title="${title}" --message="${message}" --verbose
        fi
    else
        if [ ${email} -eq 1 ]; then
            ${NTFY_LOC}ntfy-curl.sh --title="${title}" --message="${message}" --email > /dev/null
        else
            ${NTFY_LOC}ntfy-curl.sh --title="${title}" --message="${message}" > /dev/null
        fi
    fi
}