#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Error Processing Function - Code Backup
# Location: backup/code-backup/functions/error.sh
# Version 1.0.2
# Author: Conor Ryan
# Date: 2024-10-27
# Description: Error Processing Function
####

error_count=0

error_process () {
    local error_message=$1
    local state=$2

    error_file="${log_location}${error_name}"

    # Find if error log exists
    if [ ! -f "${error_file}" ]; then
        touch "${error_file}"

        if [ ! -f "${error_file}" ]; then
            echo -e "Error: Unable to create log file"
            echo -e "Error: Check permissions and try again"
            exit 1
        fi
    fi
    
    echo "Error: ${error_message}"
    echo "$(date '+%Y-%m-%d %H:%M:%S') - Error: ${error_message}" >> "${error_file}"    

    if [[ -z ${error_message} ]]; then
        echo "No error message provided. Exiting..."
        return 1
    fi

    error_count=$((error_count+1))

    if [[ -z ${state} ]]; then
        state="continue"
    fi

    if [[ ${state} == "exit" ]]; then
        log_standard "Exiting Script Early due to Error"
        log_standard "See ${error_file} for more details"

        # Get Contents of Error File
        error_contents=$(cat "${error_file}")
        send_notification "Backup Script Did not Complete Due to Error" "${error_contents}"
        exit 1;
    
    else 
        log_standard "Continuing..."
    
    fi


}