# code-backup.sh

This script looks to backup important files used for code development. The idea being that if anything were to happen time would not have to be spent about reconfiguring applications etc to their previous state.

## Script Overview

The script runs as follows:

### File Copying Generation

- Creation of date folder in specified backup folder
- Call variables from `.code.env` file in directory accompanying script
- Generating a list of Brew Formulae Installed (MacOS specific) or DPKG Packages (Debian linux specific)
- Copying hosts file
- Copying crontab of current user
- Copying crypttab / fstab (linux specific)
- Copying `.ssh` folder
- Copying shell settings file - `.zshrc` file (mac specific) and/or .bashrc file (linux specific)
- Generating a list of Visual Studio Code Extensions
- Copying Cyberduck directories and files (mac specific)
- Copying code folder (any folder specified in `.code.env` file where code is stored)
- Coping Desktop folder

All of the above files can be optionally copied by setting the corresponding variable in the `.code.env` file to 1 for true or 0 for false.

See the `.code.env.example` file for more information.

### Compression / Encryption

- A `tarball` is then made of the date folder
- The uncompressed folder is then deleted
- `tarball` is then encrypted using gpg

### File Transfer

- Encrypted `tarball` is then copied to remote using `rclone` (like AWS/OneDrive/Dropbox)
- File is then copied to storage server or local network using `rsync` or `rclone`.

### Backup Complete Notification (Optional)

- Notification is sent to the user via `ntfy-cURL` script. See the [Notifications](#notifications) section below for more information.

## Requirements

Ensure you have the following programmes installed:

- `rclone` - used to transfer to cloud storage provider
- `rsync` - transfer via ssh
- `gnupg` - for file encryption

These programmes can be easily installed on MacOS via the `homebrew` package manager or via your package repository on Linux.

```bash
brew install rclone rsync gnupg
# or
sudo apt-get install rclone rsync gnupg
```

## Setting Up Transfer Locations

This script requires the remote locations to be set up before running the script. See below for how to set up the transfer locations for `rclone`, `rsync`, and `local`.

### RClone

`rclone` is used to transfer files from the host computer to external cloud providers like Dropbox or OneDrive. Once you have rclone installed run:

```bash
rclone config
```

to set up these cloud providers. Follow the instructions in the terminal to set up the cloud provider.

When asked to name your cloud provider keep the name short and descriptive like "onedrive-website" or "dropbox-backup". You will use this name later when editing the `.env` file.

### RSync

`rsync` is used to transfer files to a remote server or local network. To set up `rsync` you will need to have an SSH key set up on the remote server.

Follow the steps here to generate an SSH key: [https://www.ssh.com/ssh/keygen/](https://www.ssh.com/ssh/keygen/) and then copy the public key to the remote server in their `~/.ssh/authorized_keys` file.

### Local Transfer

The `local` transfer is used to transfer the files locally on the server. It is also used for `--first-run` command to check that the files are being copied correctly before being encrypted and transferred.

Simply create the directory somewhere on the server and in the `.env` set the remote type to `local` with the path to the directory.

## Setting Up Encryption

Encryption is a major part of this script as it is important to keep the backed up files secure. It uses the `gnupg` programme to encrypt the files.

Assuming you have already installed `gnupg` above, follow the steps below to generate an encryption key pair:

```bash
gpg --full-generate-key 
```

1. You'll be asked what kind of key you want. Pick option 2 `DSA and Elgamal` and chose `3072` as the "keysize"

2. You'll then be asked when you want the key to expire. Ideally you want the key to last no more than a year.

3. Type your name in "Real Name"

4. Then crucially the email address...

    This is the the way the computer knows which key to use to encrypt the data.

    It doesn't have to be a real address although ideally something that is owned by you but crucially it has to be unique to the system you are currently on. Something like:

    ```bash
    home-mac@your-domain.com
    ```

    *Note: Again this does not have to be a "real" functioning email address just something you'll be able to remember*

5. No comment is required but you could put something like "for home mac backup" or something like that.

6. You'll then be asked to put a password for the private key. This is where ideally you would use a password manager to create a completely random password as "the more random a password is the harder it is to crack"

    *Note: Whatever password you use WRITE IT DOWN SOMEWHERE because if you forget it then you will not be able to decrypt your backup*

Your key is now generated! Congratulations!

## Setting up the Environment Variables

Make a copy of the `.code.env.sample` file calling it `.code.env`

```bash
cp sample.code-backup.env .code-backup.env
```

Open up the `.code.env` file in your favourite text editor following the steps as outlined in the file and replacing the environmental variables with your chosen values

```bash
nano .code-backup.env
```

Follow the instructions in the `.code.env` file to set up the environmental variables for you want the script to copy. Take special note of the `platform` specific variables which determine if the script is running on a `mac` or `linux` system.

### List of Backup Variables to Set

#### Required Variables

- `system_host`: The name of the system being backed up (no spaces, use "-" or "_").
- `script_name`: The name of the script.
- `date`: Automatically set to the current date.
- `date_time`: Automatically set to the current date and time.
- `script_debug`: Set to `0` to disable debug messages, `1` to enable.
- `log_location`: Location of the log file (must end with a `/`).
- `log_name`: Name of the log file.
- `log_level`: Level of logging (`STANDARD` or `EXTENDED`).
- `log_overwrite`: Set to `0` to append to the log file, `1` to overwrite.

#### Setting Files to Backup

- `home_folder`: Location of the user's home folder.
- `backup_folder_dir`: Absolute path of the backup folder.
- `backup_folder`: Name of the unique backup.
- `package_active`: Set to `1` for MacOS (brew), `2` for Linux (Debian APT).
- `code_folder_active`: Set to `1` to copy the code folder.
- `code_folder`: Location of the code to backup.
- `crontab_active`: Set to `1` to copy the crontab.
- `cyberduck_active`: Set to `1` to copy Cyberduck settings (MacOS only).
- `desktop_folder_active`: Set to `0` to copy the desktop folder.
- `desktop_folder`: Location of the desktop folder.
- `fstab_active`: Set to `1` to copy fstab, `2` to copy fstab & crypttab.
- `gpg_encryption`: Email for GPG encryption.
- `host_file_active`: Set to `1` to copy the host file.
- `shell_setting_active`: Set to `1` for zsh settings, `2` for bash settings.
- `ssh_folder_active`: Set to `1` to copy the SSH folder.
- `vscode_active`: Set to `1` to copy VS Code settings and extension list.
- `vscode_extensions_active`: Set to `1` to copy VS Code extensions folder.

#### (Optional) Setting UpCustom Commands

Custom Commands are being worked on and are not yet fully implemented. They will be added in a future release.

#### Setting Up Remote Locations

The script has the ability to transfer the backup tarball to an unlimited number of locations. To set up the remote follow the example below. Replace `x` with the number of the remote location Like `remote_1`, `remote_2`, etc renaming the variables accordingly.

- `remote_x_active`: Set to `1` to enable remote backup location.
- `remote_x_name`: Name of the remote location.
- `remote_x_type`: Type of remote location (`rclone`, `rsync`, or `local`).
- `remote_x_host`: Host of the remote location.
- `remote_x_path`: Path of the remote location.

Below is an example of how to set up the remote locations in the `.code-backup.env` file:

```bash
# Examples
# Remote 1 - Primary Cloud Backup

remote_1_active=0
remote_1_name="Cloud Backup - S3 Bucket"
remote_1_type="rclone"
remote_1_host="example-s3-bucket"
remote_1_path="Backups/${system_host}/"

 # Remote 2 - NAS Backup

remote_2_active=0
remote_2_name="Local NAS Backup"
remote_2_type="rsync"
remote_2_host="backup@192.168.1.234"
remote_2_path="/mnt/backup/${system_host}/"

# Remote 3 - Local Backup

remote_3_active=0
remote_3_name="Local"
remote_3_type="local"
remote_3_path="/home/user/LocalStorage/${system_host}/"
```

## Running the Script (First Time)

1. Make the script executable

    ```bash
    sudo chmod +x code-backup.sh
    ```

2. Run the script in "dry-run" mode to give an echo'd output of the script and functions to be run. No files will be copied or transferred at this time. This is useful to check sample output of the script. This is to check that the script will run expected and that the environmental variables are set correctly.

    ```bash
   ./code-backup.sh dry-run
    ```

3. If you are happy with the sample output in Step 7 then run the script "first-run" mode.

    This will run the script as normal but will not transfer the files to the remote location.

    ```bash
    ./code-backup.sh first-run
    ```

     Confirm that the backup folder contains all the necessary files and folders needed and then run delete the first-run folder:

     ```bash
     rm -rf ${local_copy_location}/*
    ```

4. Run the script fully

    ```bash
    .code-backup.sh
    ```

## Usage

The script can be run in different modes to suit various needs. Below are the available modes and options:

### Options

- `--verbose`, `-v`: Enable verbose mode. This will provide more detailed output during the script execution.
- `--help`, `-h`: Display the help message with usage information.
- `--no-output`: Disable console output (useful for cronjobs).
- `--ntfy`: Enable notifications for script completion.
- `--ntfy-errors`: Enable notifications for script errors only.
- `--email`: Enable email notifications for script completion (works only if `--ntfy` also passed).

### Modes

- `dry-run`: Run the script in dry-run mode. This mode is intended to check the script's actions without performing any actual operations. The script will not perform any actions and will exit after displaying the intended operations.
- `first-run`: Run the script in first-run mode. This mode is intended to ensure that the script is working as expected by copying the correct files and running the necessary commands before compressing, encrypting, and transferring the files to a remote server. Remote transfers (rclone, rsync) are disabled in this mode.

## Logging

The script will log all output to a file in the `logs` directory. The log file will be named `code-backup.log` where is the date the script was run.

Set the following in the `.env` file:

- `log_location`: Location of the log file (must end with a `/`).
- `log_name`: Name of the log file.
- `log_level`: Level of logging (`STANDARD` or `EXTENDED`).
- `log_overwrite`: Set to `0` to append to the log file, `1` to overwrite.

See the `.env` file for more information.

## Notifications

The script can send notifications to the user when the script has completed. This can be done via the `ntfy-curl.

`NTFY-cURL` is a simple bash script which sends a notification (and email) from your configured `ntfy` server via `cURL`. It is a simple way to send notifications to your phone or computer when the script has completed.

Find out more about the `ntfy-cURL` script by going to the [ntfy-cURL directory](../../api/ntfy-curl/README.md) in this repository.

With the `ntfy-cURL` script installed and configured, you can enable notifications by passing the `--ntfy` option to the script.

### Notify Only When Script Has Errors

To only get notification when the script has errors or completely exits unexpectedly, you can use the extra flag `--ntfy-errors`.

```bash
./code-backup.sh --ntfy-errors
```

## Scheduling Backups via Cron

The script can be scheduled to run at a specific time using `cron`.

```bash
crontab -e
```

To schedule the script to run daily at 2:00 AM, add the following line to the `crontab`:

```bash
0 2 * * * /path/to/code-backup.sh --no-output
```

To run the script with notifications enabled, add the following line to the `crontab`:

```bash
0 2 * * * /path/to/code-backup.sh --no-output --ntfy --email
```

## Notes

- Part of the script *requires admin (sudo) privileges* (if enabled) to run `CyberDuck` (macOS).
  - Will skip copy if `--no-output` flag is passed.
- Custom Commands are being worked on and are not yet fully implemented. They will be added in a future release.
