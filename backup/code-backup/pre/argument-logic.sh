#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086,SC2034
# Code Backup Script
# Location: backup/code-backup/pre/argument-logic.sh
# Version 2.3.0
# Author: Conor Ryan
# Date: 2024-10-27
# Description: Argument Logic for Code Backup Script
####

verbose="0"
help="0"
no_output="0"
ntfy="0"
ntfy_errors="0"
email="0"
test="0"


# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        --verbose|-v) verbose="1"; shift ;;
        --ntfy|-n) ntfy=1; shift ;;
        --ntfy-errors|-ne) ntfy=1; ntfy_errors=1; shift ;;
        --email|-e) email=1; shift ;;
        --test|-t) test=1; shift ;;
        --help|-h) help="1"; shift ;;
        --no-output) no_output="1"; shift ;;
        *) break ;;
    esac
done

if [[ ${help} == "1" ]]; then
    echo "Usage: $0 [options] [mode]"
    echo ""
    echo "Options:"
    echo "    --verbose, -v       Enable verbose mode"
    echo "    --ntfy, -n          Send notification when backup is complete"
    echo "    --ntfy-errors, -ne  Send notification only when errors occur"
    echo "    --email, -e         Send notification via email"
    echo "    --no-output         Disable output"
    echo "    --test, -t          Test Notification Settings"
    echo "    --help, -h          Show this help message"
    echo "Modes:"
    echo "  dry-run             Run script in dry-run mode"
    echo "  first-run           Run script in first-run mode"
    exit 0
fi

if [[ ${test} == "1" ]]; then
    echo "Sending Test Notification...";
    send_notification "test"
    exit 0
fi

if [[ ${verbose} == "1" ]]; then
    log_standard "Verbose Mode Enabled"
fi

if [[ ${no_output} == "1" ]]; then
    exec > /dev/null 2>&1
fi

if [[ -z "$1" ]]; then
    log_standard "Beginning Script Execution"

elif [[ "$1" == "dry-run" ]]; then
    log_standard "Script in dry-run mode\n\nThis is intended to be used to check that \n\nThe script will not perform any actions\n\nThe script will now exit"
    dbg="echo"
    backup_folder="/tmp"

elif [[ "$1" == "first-run" ]]; then
    log_standard "First Run Mode Enabled"
    echo -e "Script in First Run mode\n\nThis is intended to be used to ensure that the script is working as expected (that the correct files are copied and commands run) BEFORE it is compressed, encrypted and transferred to a remote server. As such REMOTE Transfers (rclone, rsync) are disabled.\n\nThis script assumes you have set your local copy destination. If you have not please do so in your '.code.env' file.\n\n"
    while true; do
        read -rp "Do you wish to continue? (y/n) " yn
        case $yn in
            [Yy]* ) echo -e "\nScript will now run in first run mode\n"; break ;;
            [Nn]* ) echo -e "\nScript will now exit\n"; exit 0 ;;
            * ) echo "Please answer yes or no." ;;
        esac
    done

fi