#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086,SC2034
# Code Backup Script
# Location: backup/code-backup/pre/transfer-validation.sh
# Version 2.0.1
# Author: Conor Ryan
# Date: 2024-10-27
# Description: Validation of Transfer Options for Code Backup Script
####

any_transfer_active=0

log_verbose "Validating transfer settings...\n"

# Validate all remotes
for ((i=1; ; i++)); do
    active_var="remote_${i}_active"
        
    # Break if we've processed all defined remotes
    if [[ -z "${!active_var}" ]]; then
        break
    fi

    if [[ ${!active_var} == 1 ]]; then
        any_transfer_active=1
        validate_remote "$i" || error_process "Failed to validate remote $i" "continue"
    fi
done

# Ensure at least one transfer is active
if [[ $any_transfer_active == 0 ]]; then
    log_verbose "No backup locations are active.\n\nFor the script to proceed please enable at least one remote in your '.code.env' file\n"
    error_process "No backup locations are active" "exit"
    exit 1
fi

log_verbose "Transfer settings validation complete\n"