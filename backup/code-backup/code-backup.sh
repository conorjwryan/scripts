#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086
# Code Backup Script
# Location: backup/code-backup/code-backup.sh
# Version 2.1.1
# Author: Conor Ryan
# Date: 2024-10-27
# Description: Backup, Encryption and Transfer of Code Files and Settings
####

script_location="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

####
# Checking Existence of .env
####

if [ ! -f "${script_location}/.code-backup.env" ]; then
    echo -e "No '.code-backup.env' file found\nMake sure that you have copied from the sample file into the script directory first before executing this script\n\ncp sample.code-backup.env .code-backup.env\n\nThe script will now exit";
    exit 1;

fi

####
# Setting Variables
####

set -o allexport
source ${script_location}/.code-backup.env
set +o allexport

####
# Functions List
####

source ${script_location}/functions/functions-list.sh

####
# Argument Logic
####

source ${script_location}/pre/argument-logic.sh

####
# Checking Validation of External/Local File Copy
####

source ${script_location}/pre/transfer-validation-logic.sh

####
# Start Script
####

log_verbose "\n${system_host} Web Code Backup\n";

echo -e "The Date/Time is ${date_time}\n";

log_verbose "Creating Date Folder ${date}\n";

${dbg} mkdir -p "${backup_folder}";

####
# Start Validation of Options
####

source ${script_location}/main/file-copy-logic.sh

####
# Encryption Process
####

source ${script_location}/main/encryption-logic.sh

####
# Transmit of File 
####

source ${script_location}/main/transfer-logic.sh

####
# Script Cleanup
####

source ${script_location}/post/cleanup-logic.sh

####
# END SCRIPT
####

log_standard "\n${system_host} Code Backup Completed\n";

if [[ ${error_count} -gt 0 ]]; then
    log_standard "Errors detected: ${error_count}"
    log_standard "Check error log for details: ${error_file}"

    if [[ $ntfy == 1 ]]; then 
        error_contents=$(cat "${error_file}")
        send_notification "part-error" "Code Backup Script Complete with Errors: ${error_contents}"
    fi

    exit 1

else
    log_verbose "No errors detected"
    if [[ $ntfy_errors == 0 ]]; then
        send_notification "success"
    fi

    exit 0
fi
