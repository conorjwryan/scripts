#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086,SC2034
# Code Backup Script
# Location: backup/code-backup/logic/transfer-logic.sh
# Version 2.0.0
# Author: Conor Ryan
# Date: 2024-10-27
# Description: Transfer Logic for Code Backup Script
####

any_transfer_processed=0
remove_source=0

log_standard "Starting file transfers...\n"

# Process all remotes
for ((i=1; ; i++)); do
    active_var="remote_${i}_active"
        
    # Break if we've processed all defined remotes
    if [[ -z "${!active_var}" ]]; then
        break
    fi

    if [[ ${!active_var} == 1 ]]; then
        any_transfer_processed=1
        process_remote "$i" || error_process "Failed to process remote $i" "continue"
            
        # Set remove_source flag if this is a remote transfer
        type_var="remote_${i}_type"
    if [[ ${!type_var} != "local" ]]; then
            remove_source=1
        fi
    fi
done

# Clean up source files if any remote transfers were done
if [[ $remove_source == 1 && $first_run != 1 ]]; then
    log_standard "Cleaning up source files\n"
    ${dbg} rm -rf "${backup_folder_dir:?}/*"
fi

if [[ $any_transfer_processed == 0 ]]; then
    log_standard "No transfers were processed\n"
    error_process "No transfers were processed" "exit"
    exit 1
fi

log_standard "All transfers completed successfully\n"