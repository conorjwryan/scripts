#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086,SC2034
# Code Backup Script
# Location: backup/code-backup/logic/encryption-logic.sh
# Version 1.0.2
# Author: Conor Ryan
# Date: 2024-10-25
# Description: Encryption Logic for Code Backup Script
####

if [[ $1 != "first-run" ]]; then

log_standard "Create TAR of folder\n";

${dbg} tar -czpf "${backup_folder}.tgz" "${backup_folder}";

log_standard "\nRemoving Folder\n";

${dbg} rm -rf "${backup_folder}";

log_standard "Encrypting TGZ\n";

${dbg} gpg --encrypt --armor -r "${gpg_encryption}" "${backup_folder}.tgz";

log_standard "Removing TGZ\n";

${dbg} rm -rf "${backup_folder}.tgz";

fi