#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2086,SC2034
# Code Backup Script
# Location: backup/code-backup/logic/file-copy-logic.sh
# Version 1.0.4
# Author: Conor Ryan
# Date: 2023-10-27
# Description: File Copy Logic for Code Backup Script
####

#### 
# Custom Commands (Set in .code.env)
####

# if [[ ${custom_command_active} == 1 ]]; then
#     log_standard "Running Custom Commands\n";

#     for i in "${!custom_command_action[@]}";
#     do
#         log_standard "Running Command ${custom_name[$i]}\n";
#         ${dbg} ${custom_command_action[i]}
#         log_standard "\nCommand ${custom_command_name[$i]} Complete\n";
#     done

# elif [[ ${custom_command_active} == 0 && ${script_debug} == 1 ]]; then
#     log_verbose "Custom Commands Skipped\n"

# fi

####
# Package Manager Installed List
####

if [[ ${package_active} == 1 ]]; then
    log_standard "Copying Brew Formulae Installed\n";

   ${dbg} brew list > "${backup_folder}/installed-brew-list.txt";

elif [[ $package_active == 2 ]]; then 

    log_standard "Copying Package Manager Installed List\n"

    ${dbg} dpkg --list > "${backup_folder}/installed-packages-list.txt";

elif [[ ${package_active} == 0 && ${script_debug} == 1 ]]; then
    log_verbose "Package Manager Installed List Skipped\n"

fi

####
# Host File Copy
####

if [[ ${host_file_active} == 1 ]]; then
    log_standard "Copying Hosts File\n";
    ${dbg} cp -a /etc/hosts "${backup_folder}/hosts.txt";

elif [[ ${host_file_active} == 0 && ${script_debug} == 1 ]]; then
    log_verbose "Host File Copy Skipped\n"

fi

####
# Crontab Copy
####

if [[ ${crontab_active} == 1 ]]; then
    log_standard "Copying List of Cronjobs to File\n";
    ${dbg} crontab -l > "${backup_folder}/crontab.txt";

elif [[ ${crontab_active} == 0 && ${script_debug} == 1 ]]; then
    log_verbose "Cronjob Copy Skipped\n"

fi

####
# SSH Folder Copy
####

if [[ ${ssh_folder_active} == 1 ]]; then
    log_standard "Copying SSH Folder\n";
    ${dbg} cp -a "${home_folder}/.ssh" "${backup_folder}";

elif [[ ${ssh_folder_active} == 0 && ${script_debug} == 1 ]]; then
    log_verbose "SSH Folder Skipped\n";

fi

####
# Shell Settings File Copy
####

if [[ ${shell_setting_active} == 1 ]]; then
    log_standard "Copying ZSH Settings File\n";
    ${dbg} cp -a "${home_folder}/.zshrc" "${backup_folder}";

elif [[ ${shell_setting_active} == 2 ]]; then
    log_standard "Copying Bash Settings File\n";
    ${dbg} cp -a "${home_folder}/.bashrc" "${backup_folder}";    

elif [[ ${shell_setting_active} == 0 && ${script_debug} == 1 ]]; then
    log_verbose -e "Shell Settings File Skipped\n"

fi

####
# FSTab / CryptTab Copy
####

if [[ ${fstab_active} == 1 ]]; then
    log_standard "Copying FSTab\n";
    ${dbg} cp -a "/etc/fstab" "${backup_folder}";

elif [[ ${fstab_active} == 2 ]]; then 
    log_standard "Copying FSTab\n";
    ${dbg} cp -a "/etc/fstab" "${backup_folder}";

    log_standard "Copying Crypttab\n";
    ${dbg} cp -a "/etc/crypttab" "${backup_folder}";

elif [[ ${fstab_active} == 0 && ${script_debug} == 1 ]]; then 
    log_verbose -e "FSTab File Skipped\n"

fi

####
# VSCode Settings / List Copy
####

if [[ ${vscode_active} == 1 ]]; then
    log_verbose "Creating VSCode Folder\n";
    mkdir "${backup_folder}/VSCode";

    log_standard "Copying VSCode Extensions List\n";
    ${dbg} code --list-extensions > "${backup_folder}/VSCode/vscode-extensions.txt";

####
# VSCode Optional Copy Extensions 
####

    if [[ $vscode_extensions_active == 1 ]]; then
        log_standard "Copying VSCode Extensions Folder\n";
        ${dbg} cp -a "${home_folder}/.vscode/extensions" "${backup_folder}/VSCode/";
    
    elif [[ ${vscode_extensions_active} == 0 && ${script_debug} == 1 ]]; then
        log_verbose "VSCode Extension Folder Full Copy Skipped\n";

    fi

    log_verbose "Copying VSCode Settings File\n"
    ${dbg} cp -a "${home_folder}/Library/Application Support/Code/User/settings.json" "${backup_folder}/VSCode/vscode-settings.json";

elif [[ ${vscode_active} == 0 && ${script_debug} == 1 ]]; then
    log_verbose "VSCode Folder Skipped\n";

fi

####
# CyberDuck Settings Copy (SUDO Required)
####
# Only process if no-output not enabled

if [[ ${no_output} == 0 ]]; then

    if [[ ${cyberduck_active} == 1 ]]; then
        log_verbose "Creating CyberDuck Folder\n";
        ${dbg} mkdir "${backup_folder}/CyberDuck";

        log_standard "Copying CyberDuck Preferences\n";
        
        ${dbg} sudo cp -a "${home_folder}/Library/Preferences/ch.sudo.cyberduck.plist" "${backup_folder}/CyberDuck";
        ${dbg} sudo cp -a "${home_folder}/Library/Group Containers/G69SCX94XU.duck/Library/Application Support/duck" "${backup_folder}/CyberDuck";

        ${dbg} sudo chown -R "$USER" "${backup_folder}/CyberDuck";

    elif [[ ${cyberduck_active} == 0 && ${script_debug} == 1 ]]; then
        log_verbose "CyberDuck Folder Skipped\n";

    fi

else
    log_verbose "No Output Enabled - CyberDuck Settings Skipped\n";

fi

####
# Main Code Folder Copy
####

if [[ ${code_folder_active} == 1 ]]; then
    log_standard "Copying Code Folder\n";
    ${dbg} cp -a "${code_folder}" "${backup_folder}";


elif [[ ${code_folder_active} == 0 && ${script_debug} == 1 ]]; then
    log_verbose "Code Folder Skipped\n"

fi

####
# Desktop Copy
####

if [[ ${desktop_folder_active} == 1 ]]; then
    log_standard "Copying Desktop Folder\n";
    ${dbg} cp -a "${desktop_folder}" "${backup_folder}";


elif [[ ${desktop_folder_active} == 0 && ${script_debug} == 1 ]]; then
    log_verbose "Desktop Folder Skipped\n"

fi
